/* 確認用フォーム */
/* delete */

$(document).on('click', '#delete', function(){
    if(!confirm('本当に削除しますか？')){
        /* キャンセルの時の処理 */
    	return false;
    }else{
        /*　OKの時の処理 */
    	$('#delete').submit();
    }
});

/* update */
$(document).on('click', '#update', function(){
    if(!confirm('本当に更新しますか？')){
        /* キャンセルの時の処理 */
    	return false;
    }else{
        /*　OKの時の処理 */
    	$('#update').submit();
    }
});
