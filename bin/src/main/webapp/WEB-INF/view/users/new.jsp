<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/error/error.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログインページ</title>
		<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="<c:url value="/resources/js/script.js" />" type="text/javascript"></script>
	</head>
	<body>
		<main>
				<div class="container-fluid">
	       			<div class="row">
	           			<div class="col-sm-7 col-sm-offset-3	 h-auto main">
							<div class="page-header">
								<h1>ユーザー新規登録</h1>
							</div>
					        <form:form modelAttribute="userForm" method="post" class="form-horizontal"
					        					action="${pageContext.request.contextPath}/user/signup">
					        	<sec:csrfInput/>
						        <form:errors path="*" element="div" cssClass="error-message-list" />
					            <form:input path="id" type="hidden" value="0" /> <br />
								<table class="table table-bordered table-hover">
									<tr>
									  	<th class="active col-sm-4"><form:label path="account" cssErrorClass="error-label">アカウント:</form:label></th>
								        <td class="form-group input-wrap"><form:input path="account" cssErrorClass="error-input"/></td>
						            </tr>
				  					<tr>
									     <th class="active col-sm-4"><form:label path="password" cssErrorClass="error-label">パスワード:</form:label></th>
									     <td class="form-group input-wrap"><form:input path="password" type="password" cssErrorClass="error-input"/></td>
									</tr>
									<tr>
							            <th class="active col-sm-4"><form:label path="confirmPassword" cssErrorClass="error-label">確認用パスワード:</form:label>
							            <td class="form-group input-wrap"><form:input path="confirmPassword" type="password" cssErrorClass="error-input"/>
									</tr>
									<tr>
							            <th class="active col-sm-4"><form:label path="name" cssErrorClass="error-label">氏名:</form:label></th>
							            <td class="form-group input-wrap"><form:input path="name" cssErrorClass="error-input"/></td>
									</tr>
									<tr>
							            <th class="active col-sm-4"><form:label path="birthday" cssErrorClass="error-label">誕生日:</form:label></th>
							            <td class="form-group input-wrap"><form:input path="birthday" type="date" cssErrorClass="error-input"/></td>
									</tr>
									<tr>
							            <th class="active col-sm-4"><form:label path="mail" cssErrorClass="error-label">メールアドレス:</form:label></th>
										<td class="form-group input-wrap"><form:input path="mail" cssErrorClass="error-input"/> <br /></td>
									</tr>
									<tr>
							            <th class="active col-sm-4"><form:label path="confirmMail" cssErrorClass="error-label">確認用メールアドレス:</form:label></th>
							            <td class="form-group input-wrap"><form:input path="confirmMail" cssErrorClass="error-input"/></td>
									</tr>
								</table><br />
								<div class="login-button-wrap">
				   					<form:button class="login-button">送信</form:button>
								</div>
					        </form:form><br />
					        <div style="margin-bottom:50px;">
						        <a href="${pageContext.request.contextPath}/login">ログインページはこちら</a><br />
								<a href="${pageContext.request.contextPath}/forgotPass">パスワードを忘れた方はこちら</a><br />
							</div>
						</div>
					</div>
				</div>
		</main>
		<jsp:include page="/WEB-INF/view/layout/footer.jsp"/>
	</body>
</html>