<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isErrorPage="true" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:include page="/WEB-INF/view/layout/layout.jsp">
	<jsp:param name="title" value="404" />
	<jsp:param name="content">
		<jsp:attribute name="value">

		    <h1>403 NotFound.</h1>
		    <p>ページが見つかりません</p>
			<a href="${pageContext.request.contextPath}/home">ホーム画面に戻る</a>

		</jsp:attribute>
	</jsp:param>
</jsp:include>