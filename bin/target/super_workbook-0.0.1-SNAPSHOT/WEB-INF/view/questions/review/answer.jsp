<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<jsp:include page="/WEB-INF/view/layout/layout.jsp">
	<jsp:param name="title" value="問題" />
	<jsp:param name="content">
		<jsp:attribute name="value">
			<div class="container-fluid">
     			<div class="row">
         			<div class="col-sm-6 col-sm-offset-3 h-auto main">
						<div class="page-header">
							<h1>今日の問題</h1>
						</div>
						<div class="answer-contents">
							<div class="answer-contents-wrap">
								<div>問題：</div>
								<pre>${question.question}</pre>
							</div><br/>
							<c:if test= "${empty nextQuestionId}">
								<form action="${pageContext.request.contextPath}/answer/${question.id}" method="post"
								 			class="answer-button-wrap">
									<sec:csrfInput/>
									<input type="submit" name="score" value="分からなかった" class="answer-button">
									<input type="submit" name="score" value="難しい" class="answer-button"><br/>
									<br/>
									<input type="submit" name="score" value="普通" class="answer-button">
									<input type="submit" name="score" value="余裕" class="answer-button"><br/>
								</form><br/>
							</c:if>
							<c:if test= "${not empty nextQuestionId}">
								<c:if test= "${nextQuestionId != 0}">
									<div class="answer-contents-wrap">
										<div>回答：</div>
										<pre>${question.answer}</pre>
									</div>
									<a href="${pageContext.request.contextPath}/answer/${nextQuestionId}" >次の問題</a>
								</c:if>
								<c:if test= "${nextQuestionId == 0}">
									<div class="answer-contents-wrap">
										<div>回答：</div>
										<pre>${question.answer}</pre>
									</div>
									<span>本日分はすべて回答しました</span><br/>
									<a href="${pageContext.request.contextPath}/home">ホームへ戻る</a>
								</c:if>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>
