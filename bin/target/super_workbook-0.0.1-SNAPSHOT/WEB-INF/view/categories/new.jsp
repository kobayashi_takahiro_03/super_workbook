<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<jsp:include page="/WEB-INF/view/layout/layout.jsp">
	<jsp:param name="title" value="カテゴリ新規作成" />
		<jsp:param name="content">
			<jsp:attribute name="value">
				<div class="container-fluid">
	       			<div class="row">
	           			<div class="col-sm-5 col-sm-offset-4	 h-auto main">
							<div class="page-header">
								<h1>カテゴリ新規作成</h1>
							</div>
					        <form:form modelAttribute="categoryForm" method="post" class="form-horizontal"
					        					 action="${pageContext.request.contextPath}/category/new">
					        	<sec:csrfInput/>
						        <form:errors path="*" element="div" cssClass="error-message-list" />
					            <form:input path="id" type="hidden" value="0" />
								<table class="table table-bordered table-hover">
  									<tr>
  										<th class="active col-sm-3"><form:label path="name" cssErrorClass="error-label">カテゴリ名：</form:label></th>
  										<td class="form-group input-wrap"><form:input path="name" cssErrorClass="error-input" /></td>
  									</tr>
								</table>
								<form:button class="button">作成</form:button>
					        </form:form><br />
					</div>
				</div>
			</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>
