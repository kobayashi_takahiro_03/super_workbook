<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="/WEB-INF/view/layout/layout.jsp">
	<jsp:param name="title" value="ユーザー一覧表示" />
	<jsp:param name="content">
		<jsp:attribute name="value">
			<div class="container-fluid">
     			<div class="row">
         			<div class="col-sm-5 col-sm-offset-4 h-auto main">
					<div class="page-header">
						<h1>ユーザー一覧表示</h1>
					</div>

					<c:forEach var="user" items="${userList}">
						<div class="admin-contents">
						  	<span>ID：${user.id}</span><br />
						  	<span>アカウント：${user.account}</span><br />
						  	<span>パスワード：</span><br />
				  		  	<span>氏名：${user.name}</span><br />
				 			<span>誕生日：${user.birthday}</span><br />
				 			<span>メールアドレス：${user.mail}</span><br />
				 			<span>権限：${user.roleName}</span><br />
							 <a href="${pageContext.request.contextPath}/admin/${user.id}/show">編集する</a>
						</div><br/>
					</c:forEach>

					</div>
				</div>
			</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>