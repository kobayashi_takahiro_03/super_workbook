<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="/WEB-INF/view/layout/layout.jsp">
	<jsp:param name="title" value="マイページ編集" />
	<jsp:param name="content">
		<jsp:attribute name="value">
			<div class="container-fluid">
       			<div class="row">
           			<div class="col-sm-7 col-sm-offset-3	 h-auto main">
						<div class="page-header">
							<h1>マイページ編集</h1>
						</div>
				        <form:form modelAttribute="userForm" method="post" class="form-horizontal"
				        					 action="${pageContext.request.contextPath}/admin/${userForm.id}/edit">
				        	<sec:csrfInput/>
					        <form:errors path="*" element="div" cssClass="error-message-list" />
				            <form:input path="id" type="hidden" /> <br />
							<table class="table table-bordered table-hover">
								<tr>
								  	<th class="active col-sm-4"><form:label path="account" cssErrorClass="error-label">アカウント:</form:label></th>
							        <td class="form-group input-wrap"><form:input path="account" cssErrorClass="error-input"/></td>
					            </tr>
			  					<tr>
								     <th class="active col-sm-4"><form:label path="password" cssErrorClass="error-label">パスワード:</form:label></th>
								     <td class="form-group input-wrap"><form:input path="password" type="password" cssErrorClass="error-input"/></td>
								</tr>
								<tr>
						            <th class="active col-sm-4"><form:label path="confirmPassword" cssErrorClass="error-label">確認用パスワード:</form:label>
						            <td class="form-group input-wrap"><form:input path="confirmPassword" type="password" cssErrorClass="error-input"/>
								</tr>
								<tr>
						            <th class="active col-sm-4"><form:label path="name" cssErrorClass="error-label">氏名:</form:label></th>
						            <td class="form-group input-wrap"><form:input path="name" cssErrorClass="error-input"/></td>
								</tr>
								<tr>
						            <th class="active col-sm-4"><form:label path="birthday" cssErrorClass="error-label">誕生日:</form:label></th>
						            <td class="form-group input-wrap"><form:input path="birthday" type="date" cssErrorClass="error-input"/></td>
								</tr>
								<tr>
						            <th class="active col-sm-4"><form:label path="mail" cssErrorClass="error-label">メールアドレス:</form:label></th>
									<td class="form-group input-wrap"><form:input path="mail" cssErrorClass="error-input"/> <br /></td>
								</tr>
								<tr>
						            <th class="active col-sm-4"><form:label path="confirmMail" cssErrorClass="error-label">確認用メールアドレス:</form:label></th>
						            <td class="form-group input-wrap"><form:input path="confirmMail" cssErrorClass="error-input"/></td>
								</tr>
								<tr>
						            <th class="active col-sm-4"><form:label path="roleName">権限:</form:label></th>
									<td class="form-group select-wrap">
										<form:select path="roleName" multiple="false">
											 <form:options items="${roleNameMap}" />
										</form:select><br>
									</td>
								</tr>
							</table><br />
							<div class="login-button-wrap">
								<form:button class="button pull-left" name="update" id="update">更新</form:button>
								<c:if test="${userForm.id != loginUser.id}">
									<form:button class="button pull-right" name="delete" id="delete">削除</form:button>
								</c:if>
							</div>
				        </form:form><br />
					</div>
				</div>
			</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>