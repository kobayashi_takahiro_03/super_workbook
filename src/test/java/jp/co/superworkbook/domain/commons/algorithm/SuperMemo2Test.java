package jp.co.superworkbook.domain.commons.algorithm;

import static org.junit.Assert.*;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import jp.co.superworkbook.domain.questions.dto.ReviewDto;


/**
 *
 * @author kobayashi.takahiro
 *
 * 365日を基準とし、この日程を超える回数をテストしている
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:META-INF/spring/application-config.xml",
        "classpath:META-INF/spring/security-config.xml",
        "classpath:META-INF/mybatis/mybatis-config.xml" })
@Transactional
public class SuperMemo2Test {

	ReviewDto reviewDto;

	@Before
	public void init() {
		ReviewDto reviewDto = new ReviewDto();
		this.reviewDto = reviewDto.initValue();
	}

	@Test
	public void スコアが1を10回取っても1日後を返す() throws Exception {

		SuperMemo2 superMemo2 = new SuperMemo2();
	    long millisecondsInDay = 60 * 60 * 24 * 1000;
	    long now = System.currentTimeMillis();

	   Timestamp expectedTimeStamp = new Timestamp(now + (millisecondsInDay * 1));
	   Timestamp actualTimeStamp = null;

		for(int i = 1; i <= 10; i++) {
			actualTimeStamp = superMemo2.calculateReviewDate(this.reviewDto, 1);
			assertEquals(expectedTimeStamp, actualTimeStamp);
		}
	}

	@Test
	public void スコアが2を17回取ると373日後を返す() throws Exception {
		SuperMemo2 superMemo2 = new SuperMemo2();

		for(int i = 1; i <= 17; i++) {
			this.reviewDto.setNextLearnDate(superMemo2.calculateReviewDate(this.reviewDto, 2));
		}

	    long millisecondsInDay = 60 * 60 * 24 * 1000;
	    long now = System.currentTimeMillis();

	   Timestamp expectedTimeStamp = new Timestamp(now + (millisecondsInDay * 373));
	   Timestamp actualTimeStamp = this.reviewDto.getNextLearnDate();

	   assertEquals(expectedTimeStamp, actualTimeStamp);
	}

	@Test
	public void スコアが3を12回取ると必ず406日後を返す() throws Exception {
		SuperMemo2 superMemo2 = new SuperMemo2();

		for(int i = 1; i <= 12; i++) {
			this.reviewDto.setNextLearnDate(superMemo2.calculateReviewDate(this.reviewDto, 3));
		}

	    long millisecondsInDay = 60 * 60 * 24 * 1000;
	    long now = System.currentTimeMillis();

	   Timestamp expectedTimeStamp = new Timestamp(now + (millisecondsInDay * 406));
	   Timestamp actualTimeStamp = this.reviewDto.getNextLearnDate();

	   assertEquals(expectedTimeStamp, actualTimeStamp);
	}

	@Test
	public void スコアが4を7回取ると595日後を返す() throws Exception {
		SuperMemo2 superMemo2 = new SuperMemo2();

		for(int i = 1; i <= 7; i++) {
			this.reviewDto.setNextLearnDate(superMemo2.calculateReviewDate(this.reviewDto, 4));
		}

	    long millisecondsInDay = 60 * 60 * 24 * 1000;
	    long now = System.currentTimeMillis();

	   Timestamp expectedTimeStamp = new Timestamp(now + (millisecondsInDay * 595));
	   Timestamp actualTimeStamp = this.reviewDto.getNextLearnDate();

	   assertEquals(expectedTimeStamp, actualTimeStamp);
	}

	@Test
	public void スコアが1_2_3_4を取ると9日後を返す() throws Exception {
		SuperMemo2 superMemo2 = new SuperMemo2();

		for(int i = 1; i <= 4; i++) {
			this.reviewDto.setNextLearnDate(superMemo2.calculateReviewDate(this.reviewDto, i));
			System.out.println(this.reviewDto);
		}

	    long millisecondsInDay = 60 * 60 * 24 * 1000;
	    long now = System.currentTimeMillis();

	   Timestamp expectedTimeStamp = new Timestamp(now + (millisecondsInDay * 9));
	   Timestamp actualTimeStamp = this.reviewDto.getNextLearnDate();

	   assertEquals(expectedTimeStamp, actualTimeStamp);
	}

	@Test
	public void スコアが4_3_2_1を取ると1日後を返す() throws Exception {
		SuperMemo2 superMemo2 = new SuperMemo2();

		for(int i = 4; 1 <= i; i--) {
			this.reviewDto.setNextLearnDate(superMemo2.calculateReviewDate(this.reviewDto, i));
		}

	    long millisecondsInDay = 60 * 60 * 24 * 1000;
	    long now = System.currentTimeMillis();

	   Timestamp expectedTimeStamp = new Timestamp(now + (millisecondsInDay * 1));
	   Timestamp actualTimeStamp = this.reviewDto.getNextLearnDate();

	   assertEquals(expectedTimeStamp, actualTimeStamp);
	}

}
