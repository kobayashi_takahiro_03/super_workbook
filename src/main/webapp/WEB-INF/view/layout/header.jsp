<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<header class="header-wrap">
	 <div>
	    <nav class="navbar">
	        <div class="container-fluid">
	            <div class="navbar-header pull-right">
					<ul>
						<li><a href="${pageContext.request.contextPath}/question">問題一覧</a></li>
						<li><a href="${pageContext.request.contextPath}/category">カテゴリ一覧</a></li>
						<li>
							<form action="<c:url value='/logout' />" name="logout" method="post">
									<sec:csrfInput/>
									<a href="javascript:logout.submit()">ログアウト</a>
						   </form>
						</li>
						<li><a href="${pageContext.request.contextPath}/home">ホーム</a></li>
						<li><a href="${pageContext.request.contextPath}/user/mypage">マイページ</a></li>
							 <sec:authorize url="/admin">
						 	<li><a href="${pageContext.request.contextPath}/admin">ユーザー一覧</a></li> <!-- 管理者権限にする予定 -->
						 </sec:authorize>
					</ul>
			   </div>
	        </div>
	    </nav>
    </div>
</header>