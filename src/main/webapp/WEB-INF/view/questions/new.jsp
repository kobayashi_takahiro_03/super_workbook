<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="/WEB-INF/view/layout/layout.jsp">
	<jsp:param name="title" value="問題新規作成" />
		<jsp:param name="content">
			<jsp:attribute name="value">
				<div class="container-fluid">
	       			<div class="row">
	           			<div class="col-sm-8 col-sm-offset-2 h-auto main">
							<div class="page-header">
								<h1>問題新規作成</h1>
							</div>
					        <form:form modelAttribute="questionForm" method="post" class="form-horizontal"
					        					action="${pageContext.request.contextPath}/question/new">
					        	<sec:csrfInput/>
						        <form:errors path="*" element="div" cssClass="error-message-list" />
					            <form:input path="id" type="hidden" value="0" /> <br />
								<table class="table table-bordered table-hover">
					            	<tr>
									  	<th class="active col-sm-2"><form:label path="question" cssErrorClass="error-label">問題</form:label></th>
					 			        <td class="form-group textarea-wrap"><form:textarea path="question" cssErrorClass="error-input"/></td>
						            </tr>
						            <tr>
							            <th class="active col-sm-2"><form:label path="answer" cssErrorClass="error-label">回答</form:label></th>
							            <td class="form-group textarea-wrap"><form:textarea path="answer" cssErrorClass="error-input"/></td>
						             </tr>
						             <tr>
						            	<th class="active col-sm-2"><form:label path="categories" cssErrorClass="error-label">カテゴリ</form:label></th>
								 		<td class="form-group check-box"><form:checkboxes path="categories" items="${categories}"/></td>
						 			</tr>
					 			</table>
							<form:button class="button">送信</form:button>
						</form:form>
					</div>
				</div>
			</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>
