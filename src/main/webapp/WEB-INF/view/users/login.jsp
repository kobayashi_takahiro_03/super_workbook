<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/error/error.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログインページ</title>
		<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="<c:url value="/resources/js/script.js" />" type="text/javascript"></script>
	</head>
	<body>
		<main>
				<div class="container-fluid">
	       			<div class="row">
	           			<div class="col-sm-4 col-sm-offset-4	 h-auto main">
							<div class="page-header">
								<h1>ログイン</h1>
							</div>
							<c:if test= "${not empty resultMessage}">
								<span>${resultMessage}</span><br/>
							</c:if>
							<c:if test="${param.containsKey('error')}">
								<div class="error-message-list">
									ログインIDまたはパスワードが間違っています。
								</div>
							</c:if>
							<c:url var="loginUrl" value="/login"/>
							<form:form action="${loginUrl}" method="post">
								<table class="not-certified-contentst-table login-form">
									<tr>
										<th><label for="account">ユーザー名：</label></th>
								      	<td><input name="account" type="text" style="height:25px;"></td>
								    </tr>
									<tr>
										<th><label for="password">パスワード：</label></th>
								      	<td><input type="password" id="password" name="password" style="height:25px;"></td>
									</tr>
								</table><br/>
								<div class="login-button-wrap">
									<input type="submit" value="ログイン" class="login-button">
								</div>
							</form:form><br/>
							<a href="${pageContext.request.contextPath}/user/signup">ユーザー新規登録はこちら</a><br />
							<a href="${pageContext.request.contextPath}/forgotPass">パスワードを忘れた方はこちら</a><br />
						</div>
					</div>
				</div>
		</main>
		<jsp:include page="/WEB-INF/view/layout/footer.jsp"/>
	</body>
</html>