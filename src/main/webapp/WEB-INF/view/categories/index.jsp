<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="/WEB-INF/view/layout/layout.jsp">
	<jsp:param name="title" value="カテゴリ一覧" />
	<jsp:param name="content">
		<jsp:attribute name="value">
			<div class="container-fluid">
     			<div class="row">
         			<div class="col-sm-6 col-sm-offset-3 h-auto main">
						<div class="category-contents">
							<a href="${pageContext.request.contextPath}/category/new" class="pull-right" style="font-size:20px;">カテゴリを作成する</a>
							<table class="table table-bordered table-hover">
							    <thead>
							        <tr>
							            <th class="active col-sm-2" colspan="2">カテゴリ一覧</th>
							        </tr>
							    </thead>
							    <tbody>
								    <c:forEach var="category" items="${categories}">
								        <tr>
								            <td class="form-group">${category.name}</td>
								            <td class="form-group"><a href="${pageContext.request.contextPath}/category/${category.id}/edit">編集</a></td>
								        </tr>
							        </c:forEach>
							    </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>
