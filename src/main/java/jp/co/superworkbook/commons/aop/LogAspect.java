package jp.co.superworkbook.commons.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Configuration
@EnableAspectJAutoProxy
public class LogAspect {

	@Around("execution(* *..*.*Controller.*(..))")
	public Object startLog(ProceedingJoinPoint jp) throws Throwable {

		System.out.println("コントローラーメソッド開始：" + jp.getSignature());
		try{
			Object result = jp.proceed();
			System.out.println("コントローラーメソッド終了：" + jp.getSignature());
			return result;
		}catch(Exception e){
			System.out.println("コントローラーメソッド異常終了：" + jp.getSignature());
			e.printStackTrace();
			throw e;
		}
	}

//	@Around("execution(* *..*.*Service*.*(..))")
//	public Object daoLog(ProceedingJoinPoint jp) throws Throwable{
//		System.out.println("サービスメソッド開始：" + jp.getSignature());
//		try{
//			Object result = jp.proceed();
//			System.out.println("サービスメソッド終了：" + jp.getSignature());
//			return result;
//		}catch(Exception e){
//			System.out.println("サービスメソッド異常終了：" + jp.getSignature());
//			e.printStackTrace();
//			throw e;
//		}
//	}

}
