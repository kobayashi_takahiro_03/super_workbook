package jp.co.superworkbook.commons.bean;

import java.util.Properties;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import jp.co.superworkbook.domain.commons.algorithm.CalculateReviewDateLogic;
import jp.co.superworkbook.domain.commons.algorithm.SuperMemo2;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Configuration
public class WebConfig {

  	/* メッセージ */
	@Bean
	public LocalValidatorFactoryBean localValidatorFactoryBean() {
        LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
        localValidatorFactoryBean.setValidationMessageSource(messageSource());
        return localValidatorFactoryBean;
	}

  	@Bean
	public MessageSource messageSource() {
	    ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
	    messageSource.addBasenames("classpath:META-INF/property/application-messages");
	    messageSource.setDefaultEncoding("UTF-8");
	    return messageSource;
	}

  	/* ViewResolver */
	@Bean
	public InternalResourceViewResolver internalResourceViewResolver() {
		InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
		internalResourceViewResolver.setPrefix("/WEB-INF/view/");
		internalResourceViewResolver.setSuffix(".jsp");
		return internalResourceViewResolver;
	}

	/*	メールの設定*/
	@Bean
	public JavaMailSenderImpl mailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost("smtp.gmail.com");
		mailSender.setPort(587);
		mailSender.setUsername("takachanchan2012@gmail.com");
		mailSender.setPassword("dovzhgmcjoclyidi");
		Properties property = new Properties();
		property.setProperty("mail.smtp.auth","true");
		property.setProperty("mail.smtp.starttls.enable","true");
		mailSender.setJavaMailProperties(property);
		return mailSender;
	}

	/*	復習日程のロジック*/
	@Bean
	public CalculateReviewDateLogic getCalculateReviewDateLogic() {
		CalculateReviewDateLogic calculateReviewDateLogic = new SuperMemo2();
		return calculateReviewDateLogic;
	}

	/*	マッピング*/
	@Bean
	public MapperFactory getMapperFactory(){
		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		return mapperFactory;
	}

}