package jp.co.superworkbook.commons.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import jp.co.superworkbook.domain.users.service.UserService;

@Component
public class AuthenticationEventListeners {

	private final UserService userService;

	@Autowired
	public AuthenticationEventListeners(UserService userService) {
		this.userService = userService;
	}

	@EventListener
	public void interactiveAuthenticationSuccessEvent(InteractiveAuthenticationSuccessEvent event) {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		userService.updateLoginDate(auth.getName());
	}
}