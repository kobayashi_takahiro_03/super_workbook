package jp.co.superworkbook.commons.interceptor;
//package jp.co.superworkbook.commons.filter;
//
//import java.io.IOException;
//
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//public class SessionFilter implements Filter {
//
//	  public void init(FilterConfig filterConfig) throws ServletException {
//
//	  }
//
//	  public void doFilter(ServletRequest request, ServletResponse response,
//			  FilterChain filterChain) throws IOException, ServletException {
//
//			HttpServletRequest httpRequest = (HttpServletRequest) request;
//			HttpServletResponse httpResponse = (HttpServletResponse) response;
//			HttpSession session = httpRequest.getSession();
//
//			if( ! isValidSession(httpRequest.getServletPath(), session)) {
//				session.removeAttribute("questions");
//			}
//
//	    filterChain.doFilter(request, response);
//	  }
//
//	  public void destroy() {
//	  }
//
//	 private boolean isValidSession(String servletPath, HttpSession session) {
//
//		 if( ! servletPath.matches("/home") || ! servletPath.matches("/answer.*")) {
//			 return false;
//		 }
//
//		  return true;
//	  }
//
//	}



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class SessionHandlerInterceptor extends HandlerInterceptorAdapter {

  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpSession session = httpRequest.getSession();
		session.removeAttribute("questions");
		return true;
  }

  public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
  }

  public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
  }

}