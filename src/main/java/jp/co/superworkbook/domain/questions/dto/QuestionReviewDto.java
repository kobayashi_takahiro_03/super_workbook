package jp.co.superworkbook.domain.questions.dto;

import java.util.List;

import jp.co.superworkbook.domain.categories.entity.QuestionCategory;
import jp.co.superworkbook.domain.questions.entity.Question;
import jp.co.superworkbook.domain.questions.entity.Review;
import lombok.Data;

@Data
public class QuestionReviewDto {

	private Question question;
	private Review review;
	private List<QuestionCategory> questionCategoryList;

}
