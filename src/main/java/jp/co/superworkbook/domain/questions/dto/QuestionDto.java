package jp.co.superworkbook.domain.questions.dto;

import java.sql.Timestamp;

import jp.co.superworkbook.domain.questions.entity.Learn;
import lombok.Data;

@Data
public class QuestionDto {

	private Long id;
	private String question;
	private String answer;
	private Learn isLearned;
	private String userId;
	private Timestamp createdDate;
	private Timestamp updatedDate;

}
