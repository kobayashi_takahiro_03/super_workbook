package jp.co.superworkbook.domain.questions.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.superworkbook.domain.categories.entity.Category;
import jp.co.superworkbook.domain.categories.entity.QuestionCategory;
import jp.co.superworkbook.domain.categories.mapper.CategoryMapper;
import jp.co.superworkbook.domain.categories.mapper.QuestionCategoryMapper;
import jp.co.superworkbook.domain.questions.dto.QuestionReviewDto;
import jp.co.superworkbook.domain.questions.entity.QuestionReview;
import jp.co.superworkbook.domain.questions.mapper.QuestionReviewMapper;

@Service
@Transactional
public class QuestionReviewCategoriesService {

	private final QuestionReviewMapper questionReviewMapper;
	private final QuestionCategoryMapper questionCategoryMapper;
	private final CategoryMapper categoryMapper;

	@Autowired
	public QuestionReviewCategoriesService(QuestionReviewMapper questionReviewMapper,
			QuestionCategoryMapper questionCategoryMapper, CategoryMapper categoryMapper) {
		this.questionReviewMapper = questionReviewMapper;
		this.questionCategoryMapper = questionCategoryMapper;
		this.categoryMapper = categoryMapper;
	}

	/**
	 *
	 * @param userId
	 * @return
	 *
	 * カテゴリの一覧と中間テーブル（question_cateory_categorizations）からカテゴリを取り出して、
	 * IDと名前を紐づけている。最終的にQuestionCategoryのcategoryに代入しています。
	 *
	 */

    public List<QuestionReviewDto> findByUserId(String userId){

    	List<QuestionReview> dtoList = questionReviewMapper.findByUserId(userId);
    	List<Category> categoryDtoList = categoryMapper.findByUserId(userId);

    	for(QuestionReview qr : dtoList) {
    		List<QuestionCategory> qcList = questionCategoryMapper.findByQuestionId(qr.getQuestion().getId());
    		for(QuestionCategory qc : qcList) {
    			for(Category category : categoryDtoList) {
    				if(category.getId().equals(qc.getCategoryId())) {
    					qc.setCategory(category.getName());
    				}
    			}
    		}
    		qr.setQuestionCategoryList(qcList);
    	}

        return convertToDto(dtoList);
    }

    public List<QuestionReviewDto> findByUserIdAndToday(String userId){

    	Timestamp today = new Timestamp(new Date().getTime());
    	List<QuestionReview> dtoList = questionReviewMapper.findByUserIdAndToday(userId, today);
    	for(QuestionReview qr : dtoList) {
    		List<QuestionCategory> qc = questionCategoryMapper.findByQuestionId(qr.getQuestion().getId());
    		qr.setQuestionCategoryList(qc);
    	}
        return convertToDto(dtoList);
    }

    public QuestionReviewDto findById(Long id) {
    	QuestionReviewDto dto = new QuestionReviewDto();
    	QuestionReview entity = questionReviewMapper.findById(id);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public List<QuestionReviewDto> findAll(){
    	return convertToDto(questionReviewMapper.findAll());
    }

    private List<QuestionReviewDto> convertToDto(List<QuestionReview> questionList) {
        List<QuestionReviewDto> resultList = new LinkedList<QuestionReviewDto>();
        for (QuestionReview entity : questionList) {
        	QuestionReviewDto dto = new QuestionReviewDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

}
