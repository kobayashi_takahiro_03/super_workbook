package jp.co.superworkbook.domain.questions.mapper;

import java.util.List;

import jp.co.superworkbook.domain.questions.dto.ReviewDto;
import jp.co.superworkbook.domain.questions.entity.Review;

public interface ReviewMapper {

	Review findById(Long id);
    List<Review> findAll();
    int insert(ReviewDto dto);
	int delete(Long id);
	int update(ReviewDto dto);

}
