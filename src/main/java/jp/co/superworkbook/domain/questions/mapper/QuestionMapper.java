package jp.co.superworkbook.domain.questions.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.superworkbook.domain.questions.dto.QuestionDto;
import jp.co.superworkbook.domain.questions.entity.Question;

public interface QuestionMapper {

	Question findById(Long id);
    List<Question> findAll();
    Question findByUser(QuestionDto dto);
	boolean existUserIdAndId(@Param("userId") String userId, @Param("id") Long id);
    int insert(QuestionDto dto);
	int delete(Long id);
    int deleteAll();
	int update(QuestionDto dto);

}
