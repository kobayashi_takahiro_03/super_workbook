package jp.co.superworkbook.domain.questions.entity;

import java.util.List;

import jp.co.superworkbook.domain.categories.entity.QuestionCategory;
import lombok.Data;

@Data
public class QuestionReview {

	private Question question;
	private Review review;
	private List<QuestionCategory> questionCategoryList;

}
