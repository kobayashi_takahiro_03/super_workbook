package jp.co.superworkbook.domain.questions.dto;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class ReviewDto {

	private Long id;
	private Integer repetitions;
	private Integer intervals;
	private Float easiness;
	private Integer compleateDays;
	private Timestamp nextLearnDate;
	private Timestamp createdDate;
	private Timestamp updatedDate;

	public ReviewDto initValue() {
		ReviewDto dto = new ReviewDto();
		dto.setId(0L);
		dto.setRepetitions(0);
		dto.setIntervals(1);
		dto.setEasiness(2.5F);
		dto.setCompleateDays(365);
		return dto;
	}

}
