package jp.co.superworkbook.domain.questions.entity;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class Review {

	private Long id;
	private Integer repetitions;
	private Integer intervals;
	private Float easiness;
	private Integer compleateDays;
	private Timestamp nextLearnDate;
	private Timestamp createdDate;
	private Timestamp updatedDate;

}
