package jp.co.superworkbook.domain.questions.entity;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class Question {

	private Long id;
	private String question;
	private String answer;
	private Learn isLearned;
	private String userId;
	private Timestamp createdDate;
	private Timestamp updatedDate;

}
