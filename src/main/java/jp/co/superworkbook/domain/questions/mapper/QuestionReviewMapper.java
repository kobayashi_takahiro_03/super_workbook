package jp.co.superworkbook.domain.questions.mapper;

import java.sql.Timestamp;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.superworkbook.domain.questions.dto.QuestionReviewDto;
import jp.co.superworkbook.domain.questions.entity.Question;
import jp.co.superworkbook.domain.questions.entity.QuestionReview;
import jp.co.superworkbook.domain.questions.entity.Review;

public interface QuestionReviewMapper {

	List<QuestionReview> findByUserId(String userId);
	List<QuestionReview> findByUserIdAndToday(@Param("userId") String userId, @Param("today") Timestamp today);
	List<QuestionReview> findAll();
	QuestionReview findById(Long id);

    Review findReviewById(Long id);
    Question findQuestionById(Long id);
    int insert(QuestionReviewDto dto);
	int delete(Long id);
	int update(QuestionReviewDto dto);
	Review findReviewByQuestionId(Long id);

}
