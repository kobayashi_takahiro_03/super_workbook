package jp.co.superworkbook.domain.questions.service;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.superworkbook.domain.commons.algorithm.CalculateReviewDateLogic;
import jp.co.superworkbook.domain.questions.dto.QuestionDto;
import jp.co.superworkbook.domain.questions.dto.ReviewDto;
import jp.co.superworkbook.domain.questions.entity.Learn;
import jp.co.superworkbook.domain.questions.entity.Question;
import jp.co.superworkbook.domain.questions.entity.Review;
import jp.co.superworkbook.domain.questions.mapper.QuestionMapper;
import jp.co.superworkbook.domain.questions.mapper.ReviewMapper;

@Service
@Transactional
public class ReviewService {

	private final ReviewMapper reviewMapper;
	private final QuestionMapper questionMapper;
	private final CalculateReviewDateLogic calucReviewDateLogic;

	@Autowired
	public ReviewService(ReviewMapper reviewMapper, QuestionMapper questionMapper,
			CalculateReviewDateLogic calucReviewDateLogic) {
		this.reviewMapper =  reviewMapper;
		this.questionMapper = questionMapper;
		this.calucReviewDateLogic = calucReviewDateLogic;
	}

    public int insert(QuestionDto questionDto) {
    	ReviewDto dto = new ReviewDto().initValue();
    	dto.setId(questionDto.getId());
    	return reviewMapper.insert(dto);
    }

    public ReviewDto findById(Long id) {
    	ReviewDto dto = new ReviewDto();
    	Review entity = reviewMapper.findById(id);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public int update(ReviewDto dto, int score) {
		Timestamp nextLearnDate = calucReviewDateLogic.calculateReviewDate(dto, score);
		dto.setNextLearnDate(nextLearnDate);
    	dto.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
    	checkCompleateDays(dto);
        return reviewMapper.update(dto);
    }

    public List<ReviewDto> findAll(){
        return convertToDto(reviewMapper.findAll());
    }

    private List<ReviewDto> convertToDto(List<Review> questionList) {
        List<ReviewDto> resultList = new LinkedList<ReviewDto>();
        for (Review entity : questionList) {
        	ReviewDto dto = new ReviewDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

    /**
     * @param reviewDto
     *
     * 学習が完了したかを検知
     *
     */
    private void checkCompleateDays(ReviewDto reviewDto) {
        if(reviewDto.getCompleateDays() < reviewDto.getIntervals()) {
        	QuestionDto dto = new QuestionDto();
        	Question entity = questionMapper.findById(reviewDto.getId());
            BeanUtils.copyProperties(entity, dto);
            dto.setIsLearned(Learn.Learned);
            questionMapper.update(dto);
        }
    }

}
