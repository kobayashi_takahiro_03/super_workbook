package jp.co.superworkbook.domain.questions.service;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.superworkbook.controller.questions.QuestionForm;
import jp.co.superworkbook.domain.categories.service.CategoryService;
import jp.co.superworkbook.domain.questions.dto.QuestionDto;
import jp.co.superworkbook.domain.questions.dto.ReviewDto;
import jp.co.superworkbook.domain.questions.entity.Learn;
import jp.co.superworkbook.domain.questions.entity.Question;
import jp.co.superworkbook.domain.questions.mapper.QuestionMapper;
import jp.co.superworkbook.domain.questions.mapper.ReviewMapper;


/**
 *
 * @author kobayashi.takahiro
 *
 * 例外処理をどうしましょうか？
 * メソッド全体に付けるか、クエリ実行時に付けるか。
 * 補足場所はコントローラー？等
 *
 */

@Service
@Transactional
public class QuestionService {

	private final QuestionMapper questionMapper;
	private final ReviewMapper reviewMapper;

	@Autowired
	public QuestionService(QuestionMapper questionMapper, ReviewMapper reviewMapper,
			CategoryService categoryService) {
		this.questionMapper = questionMapper;
		this.reviewMapper = reviewMapper;
	}

    List<QuestionDto> findAll(){
        return convertToDto(questionMapper.findAll());
    }

    public QuestionDto findById(Long id) {
    	QuestionDto dto = new QuestionDto();
    	Question entity = questionMapper.findById(id);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public int update(QuestionForm questionForm) {

    	QuestionDto dto = new QuestionDto();
    	dto.setId(questionForm.getId());
    	dto.setQuestion(questionForm.getQuestion());
    	dto.setAnswer(questionForm.getAnswer());
    	dto.setUpdatedDate(new Timestamp(System.currentTimeMillis()));

    	return questionMapper.update(dto);
    }

	public boolean existUserIdAndId(String userId, Long id) {
		return questionMapper.existUserIdAndId(userId, id);
	}

    public int insert(QuestionForm questionForm, String userId) throws Exception {

    	QuestionDto questionDto = new QuestionDto();
    	questionDto.setQuestion(questionForm.getQuestion());
    	questionDto.setUserId(userId);
    	questionDto.setAnswer(questionForm.getAnswer());
    	questionDto.setIsLearned(Learn.NotLearnd);

    	int result = questionMapper.insert(questionDto);
    	result += insertReview(questionDto.getId());

		/*
		 * インサートされたIDがquestionDtoに戻ってくるため、それを格納
		 */
		questionForm.setId(questionDto.getId());

		return  result;
    }

    public int delete(Long id) {
    	return questionMapper.delete(id);
    }

    private List<QuestionDto> convertToDto(List<Question> questionList) {
        List<QuestionDto> resultList = new LinkedList<QuestionDto>();
        for (Question entity : questionList) {
        	QuestionDto dto = new QuestionDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

    private int insertReview(Long questionId) {
    	ReviewDto reviewDto = new ReviewDto().initValue();
    	reviewDto.setId(questionId);
		return reviewMapper.insert(reviewDto);
    }

}