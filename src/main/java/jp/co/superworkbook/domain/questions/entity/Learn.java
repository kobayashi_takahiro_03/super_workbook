package jp.co.superworkbook.domain.questions.entity;

public enum Learn {

	    Learned(1, "完了"),
	    NotLearnd(0, "未完了"),
	    ;

	    private final int num;
	    private final String status;

	    private Learn(final int num, final String status) {
	        this.num = num;
	        this.status = status;
	    }

	    public int getInt() {
	        return this.num;
	    }

	    public String getString() {
	    	return status;
	    }

}
