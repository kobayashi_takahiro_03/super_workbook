package jp.co.superworkbook.domain.categories.entity;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class Category {

	private Long id;
	private String name;
	private String userId;
	private Timestamp createdDate;
	private Timestamp updatedDate;

}
