package jp.co.superworkbook.domain.categories.service;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.superworkbook.controller.categories.CategoryForm;
import jp.co.superworkbook.domain.categories.dto.CategoryDto;
import jp.co.superworkbook.domain.categories.entity.Category;
import jp.co.superworkbook.domain.categories.mapper.CategoryMapper;

@Service
@Transactional
public class CategoryService {

    private final CategoryMapper categoryMapper;

    @Autowired
    public CategoryService(CategoryMapper categoryMapper) {
    	this.categoryMapper = categoryMapper;
	}

    public List<CategoryDto> findAll(){
        return convertToDto(categoryMapper.findAll());
    }

	public List<CategoryDto> findByUserId(String userId) {
        return convertToDto(categoryMapper.findByUserId(userId));
	}

	public CategoryDto findByUserIdAndId(String userId ,Long id) {
		CategoryDto dto = new CategoryDto();
		Category entity = categoryMapper.findByUserIdAndId(userId, id);
        BeanUtils.copyProperties(entity, dto);
		return dto;
	}

	public int update(CategoryForm categoryForm) {
		CategoryDto dto = new CategoryDto();
		dto.setId(categoryForm.getId());
		dto.setName(categoryForm.getName());
    	dto.setUpdatedDate(new Timestamp(System.currentTimeMillis()));

		return categoryMapper.update(dto);
	}

	public int insert(CategoryForm categoryForm, String userId) {

		CategoryDto dto = new CategoryDto();
		dto.setId(categoryForm.getId());
		dto.setName(categoryForm.getName());
		dto.setUserId(userId);
		return categoryMapper.insert(dto);
	}

	public int delete(Long id) {
		return categoryMapper.delete(id);
	}

	public boolean existUserIdAndName(String userId, String name) {
		return categoryMapper.existUserIdAndName(userId, name);
	}

	public boolean existUserIdAndId(String string, Long id) {
		return categoryMapper.existUserIdAndId(string, id);
	}

    private List<CategoryDto> convertToDto(List<Category> categoryList) {
        List<CategoryDto> resultList = new LinkedList<CategoryDto>();
        for (Category entity : categoryList) {
        	CategoryDto dto = new CategoryDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

}
