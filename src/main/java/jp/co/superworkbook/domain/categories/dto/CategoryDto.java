package jp.co.superworkbook.domain.categories.dto;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class CategoryDto {

	private Long id;
	private String name;
	private String userId;
	private Timestamp createdDate;
	private Timestamp updatedDate;

}
