package jp.co.superworkbook.domain.categories.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.superworkbook.domain.categories.dto.QuestionCategoryDto;
import jp.co.superworkbook.domain.categories.entity.QuestionCategory;

public interface QuestionCategoryMapper {

	List<QuestionCategory> findByQuestionId(Long id);
	int insert(QuestionCategoryDto dto);
	int update(QuestionCategoryDto dto);
    int deleteAll(@Param("deleteIdList") List<Long> deleteIdList);

}
