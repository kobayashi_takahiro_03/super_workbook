package jp.co.superworkbook.domain.categories.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.superworkbook.domain.categories.dto.CategoryDto;
import jp.co.superworkbook.domain.categories.entity.Category;

public interface CategoryMapper {

    List<Category> findAll();
    Category findByUserIdAndId(@Param("userId") String userId, @Param("id") Long id);
	boolean existUserIdAndName(@Param("userId") String userId, @Param("name") String name);
	boolean existUserIdAndId(@Param("userId") String userId, @Param("id") Long id);
	int insert(CategoryDto dto);
	int delete(Long id);
	int update(CategoryDto dto);
	List<Category> findByUserId(String userId);

}
