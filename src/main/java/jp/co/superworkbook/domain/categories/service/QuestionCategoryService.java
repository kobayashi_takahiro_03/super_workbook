package jp.co.superworkbook.domain.categories.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.superworkbook.controller.questions.QuestionForm;
import jp.co.superworkbook.domain.categories.dto.QuestionCategoryDto;
import jp.co.superworkbook.domain.categories.entity.QuestionCategory;
import jp.co.superworkbook.domain.categories.mapper.QuestionCategoryMapper;

@Service
@Transactional
public class QuestionCategoryService {

    private final QuestionCategoryMapper questionCategoryMapper;

    @Autowired
    public QuestionCategoryService(QuestionCategoryMapper questionCategoryMapper) {
    	this.questionCategoryMapper = questionCategoryMapper;
	}

    public List<QuestionCategoryDto> findByQuestionId(Long questionId){
        return convertToDto(questionCategoryMapper.findByQuestionId(questionId));
    }

    /**
     *
     * @param questionForm
     * @return 更新・インサートされた合計値
     *
     * QuestionCategoryDtoListのリストがそのまま持ってこれないので、
     * registedCategoryIdをDtoへ詰め替える作業が必要になっている。
     * for文①は、registedCategoryIdを更新用のDtoにマッピング
     * for文②は、更新前とfor文①で作った値を比較して、インサートするか調べるために定義
     * for文③は、DBに保存されているカラムとインサートしようとしている値を比較し、
     * 削除するカラムを決定するために定義
     *
     */
    public int insertUpdateAll(QuestionForm questionForm) {

    	List<QuestionCategoryDto> updateDto = new ArrayList<QuestionCategoryDto>();

    	for(Long value : questionForm.getCategories()) { //①
    		QuestionCategoryDto dto = new QuestionCategoryDto();
    		dto.setId(0L);
    		dto.setQuestionId(questionForm.getId());
    		dto.setCategoryId(value);
    		updateDto.add(dto);
    	}

    	List<QuestionCategoryDto> updateBeforeDto = questionForm.getQuestionCategoryDtoList();

    	for(int i = 0; i < updateDto.size(); i++) { //②
    		for(QuestionCategoryDto dto : updateBeforeDto) {
        		if(updateDto.get(i).getCategoryId().equals(dto.getCategoryId())) {
        			updateDto.get(i).setId(dto.getId());
        		}
    		}
    	}

    	List<QuestionCategoryDto> currentDto = findByQuestionId(questionForm.getId());
    	List<Long> deleteIdList = new ArrayList<Long>();

		for(QuestionCategoryDto cDto  : currentDto) { //③
			if( ! updateDto.contains(cDto)) {
				deleteIdList.add(cDto.getId());
			}
		}

		if( ! deleteIdList.isEmpty()) {
			questionCategoryMapper.deleteAll(deleteIdList);
		}

		if(updateDto.isEmpty()) {
			return 0;
		}

		int result = 0;
		for(QuestionCategoryDto dto : updateDto) {
			if(dto.getId() == 0) {
				result += questionCategoryMapper.insert(dto);
			}
		}

    	return result;
    }

    public int insertAll(QuestionForm questionForm) {

    	List<Long> categoryList = questionForm.getCategories();

    	if(categoryList.isEmpty()) {
    		return 0;
    	}

    	int result = 0;
    	for(Long value : categoryList) {
    		QuestionCategoryDto dto = new QuestionCategoryDto();
    		dto.setId(0L);
    		dto.setQuestionId(questionForm.getId());
    		dto.setCategoryId(value);
    		questionCategoryMapper.insert(dto);
    		result++;
    	}

    	return result;
    }

    private List<QuestionCategoryDto> convertToDto(List<QuestionCategory> questionList) {
        List<QuestionCategoryDto> resultList = new LinkedList<QuestionCategoryDto>();
        for (QuestionCategory entity : questionList) {
        	QuestionCategoryDto dto = new QuestionCategoryDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }
}
