package jp.co.superworkbook.domain.categories.dto;

import lombok.Data;

@Data
public class QuestionCategoryDto {

	private Long id;
	private Long questionId;
	private Long categoryId;
	private String category;

}
