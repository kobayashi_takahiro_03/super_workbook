package jp.co.superworkbook.domain.categories.entity;

import lombok.Data;

@Data
public class QuestionCategory {

	private Long id;
	private Long questionId;
	private Long categoryId;
	private String category;

}
