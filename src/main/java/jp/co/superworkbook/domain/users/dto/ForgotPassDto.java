package jp.co.superworkbook.domain.users.dto;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class ForgotPassDto {

	private String uuid;
	private String mail;
	private Timestamp createdDate;
	private Timestamp updatedDate;

}
