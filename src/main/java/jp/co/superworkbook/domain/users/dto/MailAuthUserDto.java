package jp.co.superworkbook.domain.users.dto;

import java.sql.Date;
import java.sql.Timestamp;

import jp.co.superworkbook.domain.users.entity.RoleName;
import lombok.Data;

@Data
public class MailAuthUserDto {

	private String uuid;
	private String account;
	private String password;
	private String name;
	private String mail;
	private Date birthday;
	private RoleName roleName;
	private Timestamp createdDate;
	private Timestamp updatedDate;

}
