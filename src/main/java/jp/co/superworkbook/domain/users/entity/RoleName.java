package jp.co.superworkbook.domain.users.entity;

public enum RoleName {

	ROLE_ADMIN("ROLE_ADMIN"),
	ROLE_USER("ROLE_USER"),
    ;

    private final String role;

    private RoleName(final String role) {
        this.role = role;
    }

    public String getString() {
    	return role;
    }

}
