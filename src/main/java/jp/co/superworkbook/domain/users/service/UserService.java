package jp.co.superworkbook.domain.users.service;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import jp.co.superworkbook.controller.users.UserForm;
import jp.co.superworkbook.controller.users.forgotPass.ForgotPassForm;
import jp.co.superworkbook.domain.users.dto.MailAuthUserDto;
import jp.co.superworkbook.domain.users.dto.UserDto;
import jp.co.superworkbook.domain.users.entity.RoleName;
import jp.co.superworkbook.domain.users.entity.User;
import jp.co.superworkbook.domain.users.mapper.UserMapper;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;

@Service
@Transactional
public class UserService {

    private final UserMapper userMapper;
    private final MapperFactory mapperFactory;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserMapper userMapper, MapperFactory mapperFactory,
    		JavaMailSenderImpl sender,PasswordEncoder passwordEncoder) {
    	this.userMapper = userMapper;
    	this.mapperFactory = mapperFactory;
    	this.passwordEncoder = passwordEncoder;
	}

    public UserDto findById(String id) {
        UserDto dto = new UserDto();
        User entity = userMapper.findById(id);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public List<UserDto> findAll(){
        return convertToDto(userMapper.findAll());
    }

    public UserDto findByUser(UserDto dto) {
        User entity = userMapper.findByUser(dto);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public UserDto findByMail(String mail) {
    	UserDto dto = new UserDto();
        User entity = userMapper.findByMail(mail);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public UserDto findByAccount(String account) {
    	UserDto dto = new UserDto();
        User entity = userMapper.findByAccount(account);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public int insert(MailAuthUserDto mailAuthUserDto) {

    	BoundMapperFacade<MailAuthUserDto, UserDto> bmf =
    	        mapperFactory.getMapperFacade(MailAuthUserDto.class, UserDto.class);
    	UserDto dto = bmf.map(mailAuthUserDto);
		String uuid = String.format("%040d", new BigInteger(UUID.randomUUID().toString().replace("-", ""), 16));
		dto.setId(uuid);
    	dto.setPassword(passwordEncoder.encode(dto.getPassword()));
    	dto.setBirthday(mailAuthUserDto.getBirthday());
    	dto.setRoleName(RoleName.ROLE_USER);

        int count = userMapper.insert(dto);

        return count;
    }

    public boolean existUserId(String id) {
    	return userMapper.existUserId(id);
    }

    public boolean existUserMail(String mail) {
    	return userMapper.existUserMail(mail);
    }

    public boolean existUserAccount(String account) {
    	return userMapper.existUserAccount(account);
    }

    public int delete(String id) {
        int count = userMapper.delete(id);
        return count;
    }

    public int update(UserForm userForm) {

    	BoundMapperFacade<UserForm, UserDto> bmf =
    	        mapperFactory.getMapperFacade(UserForm.class, UserDto.class);
    	UserDto dto = bmf.map(userForm);

    	if(StringUtils.isNotBlank(userForm.getPassword())) {
        	dto.setPassword(passwordEncoder.encode(userForm.getPassword()));
    	}else {
    		String password = findById(userForm.getId()).getPassword();
    		dto.setPassword(password);
    	}

		for(RoleName rn :  RoleName.values()) {
			if(rn.getString().equals(dto.getRoleName().getString())) {
				dto.setRoleName(rn);
			}
		}
    	dto.setUpdatedDate(new Timestamp(System.currentTimeMillis()));

        return userMapper.update(dto);
    }

    public int updateUserPass(ForgotPassForm forgotPassForm) {

    	UserDto dto = new UserDto();
    	dto.setId(forgotPassForm.getId());
    	dto.setPassword(passwordEncoder.encode(forgotPassForm.getPassword()));
    	dto.setUpdatedDate(new Timestamp(System.currentTimeMillis()));

        return userMapper.updateUserPass(dto);
    }

    public int updateLoginDate(String account) {

    	UserDto dto = findByAccount(account);
    	dto.setLoginDate(new Timestamp(System.currentTimeMillis()));
    	return userMapper.updateLoginDate(dto);
    }

	/**
	 *
	 * @param result
	 * @param userForm
	 * @return
	 *
	 * アップデート時に確認用パスワード、確認用メールアドレスが空っぽでも更新出来るようにする。
	 * そのためのフィルター
	 *
	 */

	public BindingResult filteringBindingResult(BindingResult result, UserForm userForm) {

		UserDto userDto = findById(userForm.getId());
		BindingResult tmpResult = new BeanPropertyBindingResult(userForm, "userForm");

		if(StringUtils.isEmpty(userForm.getPassword()) && userDto.getMail().equals(userForm.getMail())) {

			for(FieldError fieldError : result.getFieldErrors()) {
				if(fieldError.getField().equals("confirmMail") ||
						fieldError.getField().equals("confirmPassword") ||
						fieldError.getField().equals("password")) {
					continue;
				}
				tmpResult.addError(fieldError);
			}

			return tmpResult;
		}

		if(StringUtils.isEmpty(userForm.getPassword())) {

			for(FieldError fieldError : result.getFieldErrors()) {
				if(fieldError.getField().equals("confirmPassword") ||
						fieldError.getField().equals("password")) {
					continue;
				}
				tmpResult.addError(fieldError);
			}

			return tmpResult;
		}

		if(userDto.getMail().equals(userForm.getMail())) {

			for(FieldError fieldError : result.getFieldErrors()) {
				if(fieldError.getField().equals("confirmMail")) {
					continue;
				}
				tmpResult.addError(fieldError);
			}

			return tmpResult;
		}

		return result;
	}

    private List<UserDto> convertToDto(List<User> UserList) {
        List<UserDto> resultList = new LinkedList<UserDto>();
        for (User entity : UserList) {
            UserDto dto = new UserDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

}