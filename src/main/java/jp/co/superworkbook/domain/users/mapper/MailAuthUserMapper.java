package jp.co.superworkbook.domain.users.mapper;

import java.util.List;

import jp.co.superworkbook.domain.users.dto.MailAuthUserDto;
import jp.co.superworkbook.domain.users.entity.MailAuthUser;
import jp.co.superworkbook.domain.users.entity.User;

public interface MailAuthUserMapper {

    MailAuthUser findById(String uuid);
    List<User> findAll();
    boolean existUuid(String uuid);

    MailAuthUser findByUser(MailAuthUserDto dto);
    int insert(MailAuthUserDto dto);
	int delete(Long uuid);
	int update(MailAuthUserDto dto);

}
