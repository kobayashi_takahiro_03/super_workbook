package jp.co.superworkbook.domain.users.service;

import java.sql.Date;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.superworkbook.controller.users.UserForm;
import jp.co.superworkbook.domain.users.dto.MailAuthUserDto;
import jp.co.superworkbook.domain.users.entity.MailAuthUser;
import jp.co.superworkbook.domain.users.entity.RoleName;
import jp.co.superworkbook.domain.users.mapper.MailAuthUserMapper;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;

@Service
@Transactional
public class MailAuthUserService {

	private final MailAuthUserMapper mailAuthUserMapper;
    private final MapperFactory mapperFactory;
    private final JavaMailSenderImpl sender;

	@Autowired
	public MailAuthUserService(MailAuthUserMapper mailAuthUserMapper,
			JavaMailSenderImpl sender,  MapperFactory mapperFactory) {
		this.mailAuthUserMapper = mailAuthUserMapper;
		this.mapperFactory = mapperFactory;
    	this.sender = sender;
	}

	public MailAuthUserDto findByUuid(String uuid) {
		MailAuthUserDto dto = new MailAuthUserDto();
		MailAuthUser entity = mailAuthUserMapper.findById(uuid);
        BeanUtils.copyProperties(entity, dto);
        return dto;
	}

    public boolean existUuid(String uuid) {
    	return mailAuthUserMapper.existUuid(uuid);
    }

	public void authUserbyMail(UserForm userForm) {

		BoundMapperFacade<UserForm, MailAuthUserDto> bmf =
		        mapperFactory.getMapperFacade(UserForm.class, MailAuthUserDto.class);
		MailAuthUserDto mailAuthUserDto = bmf.map(userForm);
		mailAuthUserDto.setUuid(UUID.randomUUID().toString());
		mailAuthUserDto.setBirthday(Date.valueOf(userForm.getBirthday()));
		mailAuthUserDto.setRoleName(RoleName.ROLE_USER);

//		String hostname = "localhost:8080/";
//		String domain = "super_workbook/";
		String domain = "superworkbook.herokuapp.com/";
		String from = "takachanchan2012@gmail.com";
		String title = "super_workbook アカウント確認のお願い";
		String content =
				  mailAuthUserDto.getName() + "さん" + "\n" + "\n" + "以下のリンクにアクセスしてアカウントを認証してください" + "\n" + "\n"
//		          +"http://" + hostname + domain + "user/validate/" + mailAuthUserDto.getUuid()
		          +"http://" + domain + "user/validate/" + mailAuthUserDto.getUuid()
		          + "\n" + "\n"
		          + "心当たりの無い方は、メールを削除して下さい。";

		try {
	          SimpleMailMessage msg = new SimpleMailMessage();
	          msg.setFrom(from);
	          msg.setTo(mailAuthUserDto.getMail());
	          msg.setSubject(title);
	          msg.setText(content);
	          sender.send(msg);
		} catch (Exception e) {
			e.printStackTrace();
		}

		insert(mailAuthUserDto);

	}

	private int insert(MailAuthUserDto dto) {
        return mailAuthUserMapper.insert(dto);
	}


}
