package jp.co.superworkbook.domain.users.mapper;

import jp.co.superworkbook.controller.users.forgotPass.ForgotPassMailForm;
import jp.co.superworkbook.domain.users.entity.ForgotPass;

public interface ForgotPassMapper {

	int insert(ForgotPassMailForm passForm);
	boolean existUuid(String uuid);
	int deleteByUuid(String uuid);
	ForgotPass findByUuid(String uuid);

}
