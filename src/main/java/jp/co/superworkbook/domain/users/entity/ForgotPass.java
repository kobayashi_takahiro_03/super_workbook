package jp.co.superworkbook.domain.users.entity;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class ForgotPass {

	private String uuid;
	private String mail;
	private Timestamp createdDate;
	private Timestamp updatedDate;

}
