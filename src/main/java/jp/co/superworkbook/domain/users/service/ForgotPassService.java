package jp.co.superworkbook.domain.users.service;

import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.superworkbook.controller.users.forgotPass.ForgotPassMailForm;
import jp.co.superworkbook.domain.users.dto.ForgotPassDto;
import jp.co.superworkbook.domain.users.dto.UserDto;
import jp.co.superworkbook.domain.users.entity.ForgotPass;
import jp.co.superworkbook.domain.users.entity.User;
import jp.co.superworkbook.domain.users.mapper.ForgotPassMapper;
import jp.co.superworkbook.domain.users.mapper.UserMapper;

@Service
@Transactional
public class ForgotPassService {

	private final ForgotPassMapper forgotPassMapper;
	private final UserMapper userMapper;
    private final JavaMailSenderImpl sender;

	@Autowired
	public ForgotPassService( ForgotPassMapper forgotPassMapper,
			UserMapper userMapper, JavaMailSenderImpl sender) {
    	this.forgotPassMapper = forgotPassMapper;
    	this.userMapper = userMapper;
    	this.sender = sender;
	}

    public boolean existUuid(String uuid) {
    	return forgotPassMapper.existUuid(uuid);
    }

    public ForgotPassDto findByUuid(String uuid) {
    	ForgotPassDto dto = new ForgotPassDto();
        ForgotPass entity = forgotPassMapper.findByUuid(uuid);
        BeanUtils.copyProperties(entity, dto);
    	return dto;
    }

    public int deleteByUuid(String uuid) {
    	return forgotPassMapper.deleteByUuid(uuid);
    }

	public void authUserbyMail(ForgotPassMailForm forgotPassMailForm) {

		UserDto userDto = getUserDto(forgotPassMailForm.getMail());
		forgotPassMailForm.setUuid(UUID.randomUUID().toString());

//		String hostname = "localhost:8080/";
//		String domain = "super_workbook/";
		String domain = "superworkbook.herokuapp.com/";
		String from = "takachanchan2012@gmail.com";
		String title = "super_workbook パスワード変更のお願い";
		String content =
				userDto.getName() + "さん" + "\n" + "\n" + "以下のリンクにアクセスしてパスワードを変更してください" + "\n" + "\n"
//		          +"http://" + hostname + domain + "forgotPass/validate/" + forgotPassMailForm.getUuid()
		          +"http://" + domain + "forgotPass/validate/" + forgotPassMailForm.getUuid()
		          + "\n" + "\n"
		          + "心当たりの無い方は、メールを削除して下さい。";

		try {
	          SimpleMailMessage msg = new SimpleMailMessage();
	          msg.setFrom(from);
	          msg.setTo(userDto.getMail());
	          msg.setSubject(title);
	          msg.setText(content);
	          sender.send(msg);
		} catch (Exception e) {
			e.printStackTrace();
		}

		insert(forgotPassMailForm);
	}

	private int insert(ForgotPassMailForm forgotPassMailForm) {
		return forgotPassMapper.insert(forgotPassMailForm);
	}

	private UserDto getUserDto(String mail) {
		UserDto dto = new UserDto();
        User entity = userMapper.findByMail(mail);
        BeanUtils.copyProperties(entity, dto);
        return dto;
	}


}
