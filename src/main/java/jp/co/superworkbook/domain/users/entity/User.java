package jp.co.superworkbook.domain.users.entity;

import java.sql.Date;
import java.sql.Timestamp;

import lombok.Data;

@Data
public class User {

	private String id;
	private String account;
	private String password;
	private String name;
	private Date birthday;
	private String mail;
	private RoleName roleName;
	private Timestamp loginDate;
	private Timestamp createdDate;
	private Timestamp updatedDate;

}
