package jp.co.superworkbook.domain.users.mapper;

import java.util.List;

import jp.co.superworkbook.domain.users.dto.UserDto;
import jp.co.superworkbook.domain.users.entity.User;

public interface UserMapper {

    User findById(String id);
    User findByMail(String mail);
    User findByAccount(String account);
    List<User> findAll();

    boolean existUserId(String id);
    boolean existUserMail(String mail);
    boolean existUserAccount(String account);

    User findByUser(UserDto dto);
    int insert(UserDto dto);
	int delete(String id);

	int update(UserDto dto);
	int updateUserPass(UserDto dto);
	int updateLoginDate(UserDto dto);
}