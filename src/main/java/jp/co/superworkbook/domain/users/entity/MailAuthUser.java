package jp.co.superworkbook.domain.users.entity;

import java.sql.Date;
import java.sql.Timestamp;

import lombok.Data;

@Data
public class MailAuthUser {

	private String uuid;
	private String account;
	private String password;
	private String name;
	private String mail;
	private Date birthday;
	private RoleName roleName;
	private Timestamp createdDate;
	private Timestamp updatedDate;

}
