package jp.co.superworkbook.domain.commons.mybatis;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import jp.co.superworkbook.domain.users.entity.RoleName;

public class RoleNameTypeHandler extends BaseTypeHandler<RoleName>{

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, RoleName parameter, JdbcType jdbcType)
			throws SQLException {
		ps.setString(i, parameter.getString());
	}

	@Override
	public RoleName getNullableResult(ResultSet rs, String columnName) throws SQLException {

		for(RoleName roleName  : RoleName.values()) {
			if(rs.getString(columnName).equals(roleName.getString())) {
				return roleName;
			}
		}

		return RoleName.ROLE_USER;
	}

	@Override
	public RoleName getNullableResult(ResultSet rs, int columnIndex) throws SQLException {

		for(RoleName roleName  : RoleName.values()) {
			if(rs.getString(columnIndex).equals(roleName.getString())) {
				return roleName;
			}
		}

		return RoleName.ROLE_USER;
	}

	@Override
	public RoleName getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {

		for(RoleName roleName  : RoleName.values()) {
			if(cs.getString(columnIndex).equals(roleName.getString())) {
				return roleName;
			}
		}

		return RoleName.ROLE_USER;
	}

}
