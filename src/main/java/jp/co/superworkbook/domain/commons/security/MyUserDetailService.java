package jp.co.superworkbook.domain.commons.security;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.superworkbook.domain.users.entity.RoleName;
import jp.co.superworkbook.domain.users.entity.User;
import jp.co.superworkbook.domain.users.mapper.UserMapper;

@Service
@Transactional
public class MyUserDetailService implements UserDetailsService{

	@Autowired
	private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String account)
            throws UsernameNotFoundException {
        try {
            User user = userMapper.findByAccount(account);
            return new UserDetail(user, getAuthorities(user));
		} catch (Exception e) {
			throw new UsernameNotFoundException("user not found", e);
		}
    }

    private Collection<GrantedAuthority> getAuthorities(User user){
    	if(user.getRoleName().equals(RoleName.ROLE_ADMIN)) {
    		return AuthorityUtils.createAuthorityList("ROLE_ADMIN", "ROLE_USER");
    	}else {
    		return AuthorityUtils.createAuthorityList("ROLE_USER");
    	}
    }

}
