package jp.co.superworkbook.domain.commons.algorithm;

import java.sql.Timestamp;

import jp.co.superworkbook.domain.questions.dto.ReviewDto;

public class SuperMemo2 implements CalculateReviewDateLogic {

	public Timestamp calculateReviewDate(ReviewDto reviewDto, int quality) {
	    if (quality < 1) {
	    	quality = 1;
	    }else if(quality > 5) {
	    	quality = 4;
	    }

	    int repetitions = reviewDto.getRepetitions();
	    float easiness = reviewDto.getEasiness();
	    int interval = reviewDto.getIntervals();

	    easiness = (float) Math.max(1.3, easiness + 0.1 - (5.0 - quality) * (0.08 + (5.0 - quality) * 0.02));

	    /**
	     * defaultはqualityの値は3以下だが、1だった時、インターバルが初日にリセットされるので、甘く設定
	     */
	    if (quality < 2) {
	        repetitions = 0;
	    } else {
	        repetitions += 1;
	    }

	    if (repetitions <= 1) {
	        interval = 1;
	    } else if (repetitions == 2) {
	        interval = 6;
	    } else {
	        interval = Math.round(interval * easiness);
	    }

	    long millisecondsInDay = 60 * 60 * 24 * 1000;
	    long now = System.currentTimeMillis();

	    reviewDto.setRepetitions(repetitions ++);
	    reviewDto.setEasiness(easiness);
	    reviewDto.setIntervals(interval);

	    return  new Timestamp(now + (millisecondsInDay * interval));
	}

}
