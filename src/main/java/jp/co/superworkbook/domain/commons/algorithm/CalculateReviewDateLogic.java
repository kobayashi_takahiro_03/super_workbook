package jp.co.superworkbook.domain.commons.algorithm;

import java.sql.Timestamp;

import jp.co.superworkbook.domain.questions.dto.ReviewDto;

public interface CalculateReviewDateLogic {

	Timestamp calculateReviewDate(ReviewDto reviewDto, int quality);

}
