package jp.co.superworkbook.domain.commons.mybatis;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import jp.co.superworkbook.domain.questions.entity.Learn;

public class LearnTypeHandler extends BaseTypeHandler<Learn>{

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, Learn parameter, JdbcType jdbcType)
			throws SQLException {
		ps.setInt(i, parameter.getInt());
	}

	@Override
	public Learn getNullableResult(ResultSet rs, String columnName) throws SQLException {
		if(rs.getInt(columnName) == Learn.Learned.getInt()) {
			return Learn.Learned;
		} else {
			return Learn.NotLearnd;
		}
	}

	@Override
	public Learn getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		if(rs.getInt(columnIndex) == Learn.Learned.getInt()) {
			return Learn.Learned;
		} else {
			return Learn.NotLearnd;
		}
	}

	@Override
	public Learn getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		if(cs.getInt(columnIndex) == Learn.Learned.getInt()) {
			return Learn.Learned;
		} else {
			return Learn.NotLearnd;
		}
	}

}
