package jp.co.superworkbook.controller.users.forgotPass;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import jp.co.superworkbook.controller.users.validator.ConfirmPassword;
import jp.co.superworkbook.controller.validator.ValidGroup1;
import jp.co.superworkbook.controller.validator.ValidGroup2;
import jp.co.superworkbook.controller.validator.ValidGroup3;
import jp.co.superworkbook.controller.validator.ValidGroup4;
import lombok.Data;

@Data
@ConfirmPassword(field="password", groups = ValidGroup4.class)
public class ForgotPassForm {

	private String id;

	@NotBlank(groups = ValidGroup1.class)
	@Size(min = 6, max = 12, groups = ValidGroup2.class)
	@Pattern(regexp="^[a-zA-Z0-9]+$", groups = ValidGroup3.class)
	private String password;

	@NotBlank(groups = ValidGroup1.class)
	@Size(min = 6, max = 12, groups = ValidGroup2.class)
	@Pattern(regexp="^[a-zA-Z0-9]+$", groups = ValidGroup3.class)
	private String confirmPassword;

}
