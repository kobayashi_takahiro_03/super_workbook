package jp.co.superworkbook.controller.users;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.superworkbook.controller.validator.GroupOrder;
import jp.co.superworkbook.domain.users.dto.UserDto;
import jp.co.superworkbook.domain.users.entity.RoleName;
import jp.co.superworkbook.domain.users.service.UserService;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;

@Controller
@RequestMapping("/admin")
public class AdminUserController {

	private final UserService userService;
	private final MapperFactory mapperFactory;

	@Autowired
	public AdminUserController(UserService userService,  MapperFactory mapperFactory) {
		this.userService = userService;
		this.mapperFactory = mapperFactory;
	}

	@GetMapping
	String getIndex(Model model) {
		model.addAttribute("userList", userService.findAll());
		return "adminUser/index";
	}

	@GetMapping("/{id}/show")
	String getUser(@PathVariable("id") String id, Model model) {

		model.addAttribute("userForm", getUserForm(id));
		return "adminUser/show";
	}

	@GetMapping("/{id}/edit")
	String getEdit(@PathVariable("id") String id, Model model) {

		model.addAttribute("roleNameMap", getRoleName(id));
		model.addAttribute("userForm", getUserForm(id));
		model.addAttribute("loginUser", getAuthUser());

		return "adminUser/edit";
	}

	@PostMapping(value = "/{id}/edit", params = "update")
	String update(Model model,
			@ModelAttribute("userForm") @Validated(GroupOrder.class) UserForm userForm,
			BindingResult result) {

		BindingResult filteringBindingResult = userService.filteringBindingResult(result, userForm);

		if(filteringBindingResult.hasErrors()) {
			model.addAttribute("org.springframework.validation.BindingResult.userForm", filteringBindingResult);
			return "users/edit";
		}

		userService.update(userForm);

		return "redirect:/admin";
	}

	@PostMapping(value = "/{id}/edit", params = "delete")
	String delete(Model model,
			@ModelAttribute("userForm") @Validated(GroupOrder.class) UserForm userForm,
			BindingResult result) {

		BindingResult filteringBindingResult = userService.filteringBindingResult(result, userForm);

		if(filteringBindingResult.hasErrors()) {
			model.addAttribute("errorMessages", "指定されたパラメーターは不正です");
			model.addAttribute("org.springframework.validation.BindingResult.userForm", filteringBindingResult);
			return "users/edit";
		}

		userService.delete(userForm.getId());
		return "redirect:/admin";
	}


	private Map<String, String> getRoleName(String id) {
	    UserDto loginUser = userService.findByAccount(getAuthUser().getAccount());
	    UserDto editUser = userService.findById(id);

		Map<String, String> roleNameMap = new HashMap<String, String>();
	    if(loginUser.equals(editUser)) {
	    	roleNameMap.put(editUser.getRoleName().getString(), editUser.getRoleName().getString());
	    }else {
	    	for(RoleName rn  : RoleName.values()) {
	    		roleNameMap.put(rn.getString(), rn.getString());
	    	}
	    }

	    return roleNameMap;
	}

	private UserForm getUserForm(String id) {
		UserDto user = userService.findById(id);
    	BoundMapperFacade<UserDto, UserForm> bmf =
    	        mapperFactory.getMapperFacade(UserDto.class, UserForm.class);
    	UserForm userForm = bmf.map(user);

    	userForm.setPassword("");
		for(RoleName rn :  RoleName.values()) {
			if(rn.getString().equals(user.getRoleName().getString())) {
				userForm.setRoleName(rn.getString());
			}
		}

    	return userForm;
	}

	private UserDto getAuthUser() {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String account = auth.getName();
		return userService.findByAccount(account);
	}


}
