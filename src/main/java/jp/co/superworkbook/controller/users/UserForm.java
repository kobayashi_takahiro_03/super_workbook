package jp.co.superworkbook.controller.users;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import jp.co.superworkbook.controller.users.validator.ConfirmMail;
import jp.co.superworkbook.controller.users.validator.ConfirmPassword;
import jp.co.superworkbook.controller.validator.ValidGroup1;
import jp.co.superworkbook.controller.validator.ValidGroup2;
import jp.co.superworkbook.controller.validator.ValidGroup3;
import jp.co.superworkbook.controller.validator.ValidGroup4;
import lombok.Data;

@Data
@ConfirmMail(field="mail", groups = ValidGroup3.class)
@ConfirmPassword(field="password", groups = ValidGroup4.class)
public class UserForm {

	private String id;

	@NotBlank(groups = ValidGroup1.class)
	@Size(min=6, max=20, groups = ValidGroup2.class)
	@Pattern(regexp="^[a-zA-Z0-9]+$", groups = ValidGroup3.class)
	private String account;

	@NotBlank(groups = ValidGroup1.class)
	@Size(min = 6, max = 12, groups = ValidGroup2.class)
	@Pattern(regexp="^[a-zA-Z0-9]+$", groups = ValidGroup3.class)
	private String password;

	@NotBlank(groups = ValidGroup1.class)
	@Size(min = 6, max = 12, groups = ValidGroup2.class)
	@Pattern(regexp="^[a-zA-Z0-9]+$", groups = ValidGroup3.class)
	private String confirmPassword;

	@NotBlank(groups = ValidGroup1.class)
	@Size(min=1, max=10, groups = ValidGroup2.class)
	private String name;

	@NotBlank(groups = ValidGroup1.class)
	private String birthday;

	@NotBlank(groups = ValidGroup1.class)
	@Email(groups = ValidGroup2.class)
	private String mail;

	@NotBlank(groups = ValidGroup1.class)
	@Email(groups = ValidGroup2.class)
	private String confirmMail;

	private String roleName;

}
