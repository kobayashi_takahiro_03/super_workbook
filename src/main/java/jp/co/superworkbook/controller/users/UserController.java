package jp.co.superworkbook.controller.users;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.superworkbook.controller.users.validator.UsedAccountValidator;
import jp.co.superworkbook.controller.users.validator.UsedMailValidator;
import jp.co.superworkbook.controller.validator.GroupOrder;
import jp.co.superworkbook.domain.users.dto.MailAuthUserDto;
import jp.co.superworkbook.domain.users.dto.UserDto;
import jp.co.superworkbook.domain.users.entity.RoleName;
import jp.co.superworkbook.domain.users.service.MailAuthUserService;
import jp.co.superworkbook.domain.users.service.UserService;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;

@Controller
@RequestMapping("/user")
public class UserController {

	private final UserService userService;
	private final MapperFactory mapperFactory;
	private final MailAuthUserService mailAuthUserService;
	private final UsedAccountValidator usedAccountValidator;
	private final UsedMailValidator usedMailValidator;
	private final HttpSession session;

	@Autowired
	public UserController(UserService userService,  MapperFactory mapperFactory,
			MailAuthUserService mailAuthUserService,	UsedAccountValidator usedAccountValidator,
			UsedMailValidator usedMailValidator, HttpSession session) {
		this.userService = userService;
		this.mapperFactory = mapperFactory;
		this.mailAuthUserService = mailAuthUserService;
		this.usedAccountValidator = usedAccountValidator;
		this.usedMailValidator = usedMailValidator;
		this.session = session;
	}

	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(usedAccountValidator);
        binder.addValidators(usedMailValidator);
    }

	@GetMapping("/mypage")
	String getMypage(Model model) {

    	model.addAttribute(getUserForm());
		return "users/show";
	}

	@GetMapping("/{id}/edit")
	String getEdit(Model model) {

		UserForm form = getUserForm();
    	model.addAttribute("userForm", form);
		session.setAttribute("userId", form.getId());
    	session.setAttribute("roleName", form.getRoleName());

		return "users/edit";
	}

	@PostMapping("/{id}/edit")
	String update(Model model,
			@Validated(GroupOrder.class) @ModelAttribute("userForm") UserForm userForm,
			BindingResult result) {

		BindingResult filteringBindingResult = userService.filteringBindingResult(result, userForm);

		if(filteringBindingResult.hasErrors()) {
			model.addAttribute("org.springframework.validation.BindingResult.userForm", filteringBindingResult);
			return "users/edit";
		}

		if( ! chackParams(userForm)) {
			model.addAttribute("","");
			return "users/edit";
		}

		userService.update(userForm);
		session.removeAttribute("userId");
		session.removeAttribute("roleName");

		return "redirect:/user/mypage";

	}

	@GetMapping("/signup")
	String getSignup(Model model) {
		model.addAttribute("userForm", new UserForm());
		return "users/new";
	}

	@PostMapping("/signup")
	String create(@Validated(GroupOrder.class) @ModelAttribute("userForm") UserForm userForm,
			BindingResult result, RedirectAttributes redirectAttributes) {

		if(result.hasErrors()) {
			return "users/new";
		}

		mailAuthUserService.authUserbyMail(userForm);
		redirectAttributes.addFlashAttribute("resultMessage", "メールが送信されました。メールボックスを確認して下さい。");
		return "redirect:/login";
	}

	@GetMapping("/validate/{id}")
	String getAuthUser(@PathVariable(name = "id") String uuid, RedirectAttributes redirectAttributes) {

		if(StringUtils.isEmpty(uuid) || ! mailAuthUserService.existUuid(uuid)) {
			redirectAttributes.addFlashAttribute("resultMessage", "認証URLが無効または認証URLが空になっています");
			return "redirect:/login";
		}

		MailAuthUserDto tmpUser = mailAuthUserService.findByUuid(uuid);
		userService.insert(tmpUser);
		redirectAttributes.addFlashAttribute("resultMessage", "認証されました");

		return "redirect:/login";
	}

	private boolean chackParams(UserForm userForm) {

		String roleName = (String) session.getAttribute("roleName");

		for(RoleName rn :  RoleName.values()) {
			if(rn.getString().equals(roleName)) {
				userForm.setRoleName(rn.getString());
			}
		}

		String userId = (String) session.getAttribute("userId");
		if(userId.equals(userForm.getId())) {
			return true;
		}

		return false;
	}

	private UserDto getAuthUser() {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String account = auth.getName();
		return userService.findByAccount(account);
	}

	private UserForm getUserForm() {
		UserDto user = userService.findById(getAuthUser().getId());

    	BoundMapperFacade<UserDto, UserForm> bmf =
    	        mapperFactory.getMapperFacade(UserDto.class, UserForm.class);
    	UserForm userForm = bmf.map(user);

    	userForm.setPassword("");
		for(RoleName rn :  RoleName.values()) {
			if(rn.getString().equals(user.getRoleName().getString())) {
				userForm.setRoleName(rn.getString());
			}
		}

    	return userForm;
	}

}
