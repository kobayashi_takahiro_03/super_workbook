package jp.co.superworkbook.controller.users;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

	@GetMapping("/login")
	String getLogin() {
		return "users/login";
	}

	@GetMapping("/logout")
	String logout() {
		return "redirect:login";
	}

}
