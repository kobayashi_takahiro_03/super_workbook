package jp.co.superworkbook.controller.users.forgotPass;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.superworkbook.controller.validator.GroupOrder;
import jp.co.superworkbook.domain.users.dto.UserDto;
import jp.co.superworkbook.domain.users.service.ForgotPassService;
import jp.co.superworkbook.domain.users.service.UserService;

@Controller
public class ForgotPassController {

	private final UserService userService;
	private final ForgotPassService forgotPassService;

	@Autowired
	public ForgotPassController(UserService userService, ForgotPassService forgotPassService) {
		this.userService = userService;
		this.forgotPassService = forgotPassService;
	}

	@GetMapping("/forgotPass/validate/{id}")
	String showForgotPass(@PathVariable(name = "id") String uuid, Model model) {

		if(StringUtils.isEmpty(uuid) || ! forgotPassService.existUuid(uuid)) {
			return "redirect:/login";
		}

		ForgotPassForm forgotPassForm = new ForgotPassForm();
		String mail = forgotPassService.findByUuid(uuid).getMail();
		UserDto dto = userService.findByMail(mail);
		forgotPassForm.setId(dto.getId());

		model.addAttribute("forgotPassForm", forgotPassForm);
		model.addAttribute("uuid", uuid);

		return "forgotPass/edit";
	}

	@PostMapping("/forgotPass//validate/{id}")
	String updatePass(@PathVariable(name = "id") String uuid, RedirectAttributes redirectAttributes,
			 @ModelAttribute("forgotPassForm") @Validated(GroupOrder.class) ForgotPassForm forgotPassForm,
			 BindingResult result, Model model) {

		if(StringUtils.isEmpty(uuid) || ! forgotPassService.existUuid(uuid)) {
			return "redirect:/login";
		}

		if(result.hasErrors()) {
			model.addAttribute("uuid", uuid);
			return "forgotPass/edit";
		}

		userService.updateUserPass(forgotPassForm);
		forgotPassService.deleteByUuid(uuid);

		redirectAttributes.addFlashAttribute("resultMessage", "パスワードの更新が完了しました");

		return "redirect:/login";
	}

}
