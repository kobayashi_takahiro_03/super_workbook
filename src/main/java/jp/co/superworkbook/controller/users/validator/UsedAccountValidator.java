package jp.co.superworkbook.controller.users.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import jp.co.superworkbook.controller.users.UserForm;
import jp.co.superworkbook.domain.users.service.UserService;

@Component
public class UsedAccountValidator implements Validator{

	@Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
    	UserForm userForm = (UserForm) target;

        if (StringUtils.isEmpty(userForm.getAccount()) || isValidateUpdate(userForm)) {
            return;
        }

        if (userService.existUserAccount(userForm.getAccount())) {
            errors.rejectValue("account",
					           "UsedAccountValidator.UserForm.account",
					           "Login ID is already used");
        }
    }

    private boolean isValidateUpdate(UserForm form) {

    	if( ! form.getId().equals("0")) {
    		String account = userService.findById(form.getId()).getAccount();

    		if(account.equals(form.getAccount())) {
    			return true;
    		}
    	}

    	return false;
    }

}
