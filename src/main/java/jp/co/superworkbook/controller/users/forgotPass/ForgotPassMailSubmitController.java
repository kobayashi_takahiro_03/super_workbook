package jp.co.superworkbook.controller.users.forgotPass;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.superworkbook.controller.validator.GroupOrder;
import jp.co.superworkbook.domain.users.service.ForgotPassService;
import jp.co.superworkbook.domain.users.service.UserService;

@Controller
public class ForgotPassMailSubmitController {

	private final UserService userService;
	private final ForgotPassService forgotPassService;

	@Autowired
	public ForgotPassMailSubmitController(UserService userService, ForgotPassService forgotPassService) {
		this.userService = userService;
		this.forgotPassService = forgotPassService;
	}

	@GetMapping("/forgotPass")
	String index(Model model) {

		model.addAttribute("forgotPassMailForm", new ForgotPassMailForm());
		return "forgotPass/index";
	}

	@PostMapping("/forgotPass")
	String createForgotPass(Model model,
			@ModelAttribute("forgotPassMailForm") @Validated(GroupOrder.class) ForgotPassMailForm forgotPassMailForm,
			BindingResult result,
			RedirectAttributes redirectAttributes) {

		if(result.hasErrors()) {
			return "forgotPass/index";
		}

		if( ! userService.existUserMail(forgotPassMailForm.getMail())) {
			 FieldError fieldError = new FieldError(result.getObjectName(), "mail", "登録されていないメールアドレスです");
			 result.addError(fieldError);
			 return "forgotPass/index";
		}

		forgotPassService.authUserbyMail(forgotPassMailForm);
		redirectAttributes.addFlashAttribute("resultMessage", "メールが送信されました。メールボックスを確認して下さい。");

		return "redirect:/login";
	}

}
