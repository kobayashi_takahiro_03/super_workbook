package jp.co.superworkbook.controller.users.forgotPass;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import jp.co.superworkbook.controller.validator.ValidGroup1;
import jp.co.superworkbook.controller.validator.ValidGroup2;
import lombok.Data;

@Data
public class ForgotPassMailForm {

	private String uuid;

	@NotBlank(groups = ValidGroup1.class)
	@Email(groups = ValidGroup2.class)
	private String mail;

}
