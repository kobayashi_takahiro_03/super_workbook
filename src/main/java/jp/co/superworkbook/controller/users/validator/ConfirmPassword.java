package jp.co.superworkbook.controller.users.validator;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = { ConfirmPasswordValidator.class })
@Target({ TYPE, ANNOTATION_TYPE }) // (1)
@Retention(RUNTIME)
public @interface ConfirmPassword {
    String message() default "Password address and confirmation password address are different";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    String field();
    @Target({ TYPE, ANNOTATION_TYPE })
    @Retention(RUNTIME)
    @Documented
    public @interface List {
    	ConfirmPassword[] value();
    }
}