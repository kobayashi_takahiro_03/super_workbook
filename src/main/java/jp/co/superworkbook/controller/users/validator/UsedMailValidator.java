package jp.co.superworkbook.controller.users.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import jp.co.superworkbook.controller.users.UserForm;
import jp.co.superworkbook.domain.users.service.UserService;

@Component
public class UsedMailValidator implements Validator{

	@Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

    	UserForm userForm = (UserForm) target;

        if (StringUtils.isEmpty(userForm.getMail()) || isValidateUpdate(userForm)) {
            return;
        }

        if (userService.existUserMail(userForm.getMail())) {
            errors.rejectValue("mail",
					           "UsedMailValidator.UserForm.mail",
					           "Email address is already used");
        }
    }

    private boolean isValidateUpdate(UserForm form) {

    	if( ! form.getId().equals("0")) {
    		String mail = userService.findById(form.getId()).getMail();

    		if(mail.equals(form.getMail())) {
    			return true;
    		}
    	}

    	return false;
    }


}
