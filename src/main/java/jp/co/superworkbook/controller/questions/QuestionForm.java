package jp.co.superworkbook.controller.questions;

import java.util.List;

import javax.validation.constraints.NotBlank;

import jp.co.superworkbook.domain.categories.dto.QuestionCategoryDto;
import lombok.Data;

@Data
public class QuestionForm {

	private Long id;

	@NotBlank
	private String question;
	@NotBlank
	private String answer;

	private List<QuestionCategoryDto> questionCategoryDtoList;
	private List<Long> categories;

}
