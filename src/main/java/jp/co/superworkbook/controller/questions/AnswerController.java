package jp.co.superworkbook.controller.questions;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jp.co.superworkbook.domain.questions.dto.QuestionReviewDto;
import jp.co.superworkbook.domain.questions.dto.ReviewDto;
import jp.co.superworkbook.domain.questions.service.QuestionReviewCategoriesService;
import jp.co.superworkbook.domain.questions.service.ReviewService;
import jp.co.superworkbook.domain.users.dto.UserDto;
import jp.co.superworkbook.domain.users.service.UserService;

@Controller
@RequestMapping("/answer")
public class AnswerController {

	private final QuestionReviewCategoriesService questionReviewCategoriesService;
	private final ReviewService reviewService;
	private final HttpSession session;
	private final UserService userService;

	@Autowired
	public AnswerController(QuestionReviewCategoriesService questionReviewCategoriesService,HttpSession session,
			ReviewService reviewService, UserService userService) {
		this.questionReviewCategoriesService = questionReviewCategoriesService;
		this.session = session;
		this.reviewService = reviewService;
		this.userService = userService;
	}

	@GetMapping("/{id}")
	String getAnswer(@PathVariable("id") Long questionId, Model model) {

		QuestionReviewDto questionReviewDto = questionReviewCategoriesService.findById(questionId);
		model.addAttribute("question", questionReviewDto.getQuestion());

		return "questions/review/answer";
	}

	/**
	 *
	 * @param id
	 * @param model
	 * @param scoreParam 回答のスコアを取得する。
	 * @return
	 *
	 * View側からパラメーターを取得してから、calculateReviewDateでスコアを計算
	 * reviewsテーブルに書き込みを行う
	 *
	 */

	@PostMapping("/{questionId}")
	String answer(@PathVariable("questionId") Long questionId, Model model,
			@RequestParam(name = "score", defaultValue = "") String scoreParam) {

		if(session.getAttribute("questions") == null) {
			session.setAttribute("questions", questionReviewCategoriesService.findByUserIdAndToday(getAuthUser().getId()));
		}

		@SuppressWarnings("unchecked")
		List<QuestionReviewDto> questionReviewDtoList = (List<QuestionReviewDto>) session.getAttribute("questions");
		Long nextQuestionId = 0L;

		if(questionReviewDtoList.size() != 0) {
			questionReviewDtoList.remove(0);

			if(questionReviewDtoList.size() != 0) {
				nextQuestionId = questionReviewDtoList.get(0).getQuestion().getId();
			}

		} else {
			return "redirect:/home";
		}

		int score = convertScore(scoreParam);
		ReviewDto reviewDto = reviewService.findById(questionId);
		reviewService.update(reviewDto, score);

		model.addAttribute("question", questionReviewCategoriesService.findById(questionId).getQuestion());
		model.addAttribute("nextQuestionId", nextQuestionId);

		return "questions/review/answer";
	}

	private int convertScore(String score) {

		int result = 0;
		switch (score){
			  case "分からなかった":
					result = 1;
				    break;
			  case "難しい":
					result = 2;
					break;
			  case "普通":
					result = 3;
					break;
			  case "余裕":
					result = 4;
					break;
		}
		return result;
	}

	private UserDto getAuthUser() {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String account = auth.getName();
		return userService.findByAccount(account);
	}

}
