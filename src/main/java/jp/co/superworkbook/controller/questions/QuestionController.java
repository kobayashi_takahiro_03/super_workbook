package jp.co.superworkbook.controller.questions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.superworkbook.domain.categories.dto.CategoryDto;
import jp.co.superworkbook.domain.categories.dto.QuestionCategoryDto;
import jp.co.superworkbook.domain.categories.service.CategoryService;
import jp.co.superworkbook.domain.categories.service.QuestionCategoryService;
import jp.co.superworkbook.domain.questions.dto.QuestionDto;
import jp.co.superworkbook.domain.questions.dto.QuestionReviewDto;
import jp.co.superworkbook.domain.questions.service.QuestionReviewCategoriesService;
import jp.co.superworkbook.domain.questions.service.QuestionService;
import jp.co.superworkbook.domain.users.dto.UserDto;
import jp.co.superworkbook.domain.users.service.UserService;
import ma.glasnost.orika.MapperFactory;

@Controller
@RequestMapping("/question")
public class QuestionController {

	private final CategoryService categoryService;
	private final QuestionService questionService;
    private final QuestionCategoryService questionCategoryService;
	private final QuestionReviewCategoriesService questionReviewCategoriesService;
	private final UserService userService;
	private final HttpSession session;

	@Autowired
	public QuestionController(CategoryService categoryService, QuestionService questionService,
			QuestionReviewCategoriesService questionReviewCategoriesService, MapperFactory mapperFactory,
			QuestionCategoryService questionCategoryService, UserService userService,
			HttpSession session) {
		this.categoryService = categoryService;
		this.questionService = questionService;
		this.questionCategoryService = questionCategoryService;
		this.questionReviewCategoriesService = questionReviewCategoriesService;
		this.userService = userService;
		this.session = session;
	}

	@GetMapping
	String getIndex(Model model) {

		List<QuestionReviewDto> qrList = questionReviewCategoriesService.findByUserId(getAuthUser().getId());
		model.addAttribute("questionReviews", qrList);

		return "questions/index";

	}

	/**
	 *
	 * @param id クエリパラメーター
	 * @param questionForm フォームの値
	 * @param model
	 * @return
	 *
	 * registedCategoryIdは登録済みの質問カテゴリをデフォルトでチェックするために定義
	 * QuestionCategoryDtoListのidを使って、JSP側でやりたかったけど、やりが方分からない。
	 * カテゴリ更新前の値は、QuestionCategoryDtoListに入ってる。
	 * サービス側で、カテゴリ更新項目のフィルターを掛けるために値を保持しておく。
	 * registedCategoryIdをStringにしないとバグるため、変換してる。
	 *
	 * POST通信先で、questionCategoryDtoListを使うためsessionに保存
	 *
	 */
	@GetMapping("/{id}/edit")
	String getEdit(@PathVariable("id") Long id, Model model) {

		if( ! questionService.existUserIdAndId(getAuthUser().getId(), id)) {
			return "redirect:/question";
		}

		QuestionDto questionDto = questionService.findById(id);
		QuestionForm questionForm = new QuestionForm();
		questionForm.setId(questionDto.getId());
		questionForm.setQuestion(questionDto.getQuestion());
		questionForm.setAnswer(questionDto.getAnswer());

		List<QuestionCategoryDto> qcdList = questionCategoryService.findByQuestionId(id);
		List<Long> categories = new ArrayList<Long>();

		for(QuestionCategoryDto qcd  : qcdList) {
			categories.add(qcd.getCategoryId());
		}

		questionForm.setCategories(categories);

		session.setAttribute("questionCategoryDtoList", qcdList);
		model.addAttribute("categories", getCategoryMap(getAuthUser().getId()));
		model.addAttribute("questionForm", questionForm);

		return "questions/edit";
	}

	/**
	 *
	 * @param id
	 * @param questionForm
	 * @param model
	 * @return
	 *
	 * Listのqcdは、 sqlでinsertする必要があるかどうかを判断するために、
	 * questionCategoryDtoListを更新前の状態を引き継ぎたい。
	 * sessionに保存している。それを取り出すため。定義している。
	 *
	 */

	@PostMapping(value = "/{id}/edit", params = "update")
	String update(Model model, @PathVariable("id") Long id,
			@ModelAttribute("questionForm") @Validated QuestionForm questionForm, BindingResult result) {

		if(result.hasErrors()) {
			model.addAttribute("categories", categoryService.findAll());
			model.addAttribute("questionForm", questionForm);

			return "questions/edit";
		}

		@SuppressWarnings("unchecked")
		List<QuestionCategoryDto> qcd = (List<QuestionCategoryDto>) session.getAttribute("questionCategoryDtoList");
		session.removeAttribute("questionCategoryDtoList");
		questionForm.setQuestionCategoryDtoList(qcd);

		questionService.update(questionForm);
		questionCategoryService.insertUpdateAll(questionForm);

		return "redirect:/question";
	}

	@GetMapping("/new")
	String getNew(Model model) {
		model.addAttribute("questionForm", new QuestionForm());
		model.addAttribute("categories", getCategoryMap(getAuthUser().getId()));
		return "questions/new";

	}

	/**
	 *
	 * @param questionForm
	 * @param result
	 * @param model
	 * @return
	 *
	 * questionServiceではquestionsテーブルに登録。合わせて中で、reivewsテーブルに登録している。
	 * questionCategoryServiceはquestionに紐づく、categoryが登録されていたら、
	 * questions_categories中間テーブルに登録している。
	 *
	 */

	@PostMapping("/new")
	String create(@ModelAttribute("questionForm") @Validated QuestionForm questionForm,
			BindingResult result,
			Model model) {

		 if (result.hasErrors()) {
			model.addAttribute("categories", categoryService.findByUserId(getAuthUser().getId()));
			return "questions/new";
		 }

		try {
			questionService.insert(questionForm, getAuthUser().getId());

			if( ! questionForm.getCategories().isEmpty()) {
				questionCategoryService.insertAll(questionForm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:/question";
	}

	@PostMapping(value = "/{id}/edit", params = "delete")
	String delete(Model model, @ModelAttribute("questionForm") QuestionForm questionForm) {

		if( ! questionService.existUserIdAndId(getAuthUser().getId() , questionForm.getId())) {
			model.addAttribute("errorMessages", "指定されたパラメーターは不正です");
			model.addAttribute("categories", categoryService.findByUserId(getAuthUser().getId()));
			return "questions/edit";
		}

		questionService.delete(questionForm.getId());
		return "redirect:/question";
	}

	private UserDto getAuthUser() {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String account = auth.getName();
		return userService.findByAccount(account);
	}

	private Map<Long, String> getCategoryMap(String userId) {
	    List<CategoryDto> categoryList = categoryService.findByUserId(userId);
	    Map<Long, String> categoryMap = new HashMap<Long, String>();

		for(CategoryDto dto : categoryList) {
	    	categoryMap.put(dto.getId(), dto.getName());
		}
	    return categoryMap;
	}

}
