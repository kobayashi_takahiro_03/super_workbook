package jp.co.superworkbook.controller.categories;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import jp.co.superworkbook.controller.validator.ValidGroup1;
import jp.co.superworkbook.controller.validator.ValidGroup2;
import lombok.Data;

@Data
public class CategoryForm {

	private Long id;

	@NotBlank(groups = ValidGroup1.class)
	@Size(min = 1, max = 50 , groups = ValidGroup2.class)
	private String name;

}
