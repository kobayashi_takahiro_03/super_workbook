package jp.co.superworkbook.controller.categories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.superworkbook.controller.categories.validator.UsedCategoryValidator;
import jp.co.superworkbook.controller.validator.GroupOrder;
import jp.co.superworkbook.domain.categories.dto.CategoryDto;
import jp.co.superworkbook.domain.categories.service.CategoryService;
import jp.co.superworkbook.domain.users.dto.UserDto;
import jp.co.superworkbook.domain.users.service.UserService;

@Controller
@RequestMapping("/category")
public class CategoryController {

	private final CategoryService categoryService;
	private final UserService userService;
	private final UsedCategoryValidator usedCategoryValidator;

	@Autowired
	public CategoryController(CategoryService categoryService, UsedCategoryValidator usedCategoryValidator,
			UserService userService) {
		this.categoryService =  categoryService;
    	this.usedCategoryValidator = usedCategoryValidator;
    	this.userService = userService;
	}

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(usedCategoryValidator);
    }

	@GetMapping
	String getIndex(Model model) {
		model.addAttribute("categories", categoryService.findByUserId(getAuthUser().getId()));
		return "categories/index";
	}

	@GetMapping("/{id}/edit")
	String edit(Model model, @ModelAttribute("categoryForm") CategoryForm categoryForm) {

		CategoryDto dto = categoryService.findByUserIdAndId(getAuthUser().getId() , categoryForm.getId());
		categoryForm.setId(dto.getId());
		categoryForm.setName(dto.getName());

		return "categories/edit";
	}

	@PostMapping(value = "/{id}/edit", params = "update")
	String update(Model model,
			@Validated(GroupOrder.class) @ModelAttribute("categoryForm") CategoryForm categoryForm,
			BindingResult result) {

		 if (result.hasErrors()) {
			return "categories/edit";
		 }

		if( ! categoryService.existUserIdAndId(getAuthUser().getId(), categoryForm.getId())) {
			model.addAttribute("errorMessages", "指定されたパラメーターは不正です");
			return "categories/edit";
		}

		categoryService.update(categoryForm);

		return "redirect:/category";
	}

	@GetMapping("/new")
	String getNew(Model model) {
		model.addAttribute("categoryForm", new CategoryForm());
		return "categories/new";
	}

	@PostMapping("/new")
	String create(Model model,
			@Validated(GroupOrder.class) @ModelAttribute("categoryForm") CategoryForm categoryForm,
			BindingResult result) {

		 if (result.hasErrors()) {
			return "categories/new";
		 }

		categoryService.insert(categoryForm, getAuthUser().getId());
		return "redirect:/category";
	}

	@PostMapping(value = "/{id}/edit", params = "delete")
	String delete(Model model, @ModelAttribute("categoryForm") CategoryForm categoryForm) {

		if( ! categoryService.existUserIdAndId(getAuthUser().getId(), categoryForm.getId())) {
			model.addAttribute("errorMessages", "指定されたパラメーターは不正です");
			return "categories/edit";
		}

		categoryService.delete(categoryForm.getId());

		return "redirect:/category";
	}

	private UserDto getAuthUser() {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String account = auth.getName();
		return userService.findByAccount(account);
	}

}