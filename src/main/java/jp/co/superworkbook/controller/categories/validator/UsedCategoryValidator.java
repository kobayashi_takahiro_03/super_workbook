package jp.co.superworkbook.controller.categories.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import jp.co.superworkbook.controller.categories.CategoryForm;
import jp.co.superworkbook.domain.categories.service.CategoryService;
import jp.co.superworkbook.domain.users.dto.UserDto;
import jp.co.superworkbook.domain.users.service.UserService;

@Component
public class UsedCategoryValidator implements Validator{

	@Autowired
    private CategoryService categoryService;

	@Autowired
	private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return CategoryForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
    	CategoryForm form = (CategoryForm) target;

        if (StringUtils.isEmpty(form.getName()) || isValidateUpdate(form)) {
            return;
        }

        if (categoryService.existUserIdAndName(getAuthUser().getId(), form.getName())) {
            errors.rejectValue("name",
					           "UnUsedCategoryValidator.CategoryForm.name",
					           "カテゴリは既に登録されています。");
        }
    }

    private boolean isValidateUpdate(CategoryForm form) {

    	if(form.getId() != 0L) {
    		String name = categoryService.findByUserIdAndId(getAuthUser().getId(), form.getId()).getName();

    		if(name.equals(form.getName())) {
    			return true;
    		}
    	}

    	return false;
    }

	private UserDto getAuthUser() {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String account = auth.getName();
		return userService.findByAccount(account);
	}


}
