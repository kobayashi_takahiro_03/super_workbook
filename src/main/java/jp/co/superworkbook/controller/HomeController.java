package jp.co.superworkbook.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import jp.co.superworkbook.domain.questions.dto.QuestionReviewDto;
import jp.co.superworkbook.domain.questions.service.QuestionReviewCategoriesService;
import jp.co.superworkbook.domain.users.dto.UserDto;
import jp.co.superworkbook.domain.users.service.UserService;

@Controller
public class HomeController {

	private final HttpSession session;
	private final UserService userService;
	private final QuestionReviewCategoriesService questionReviewCategoriesService;

	public HomeController(HttpSession session, UserService userService, QuestionReviewCategoriesService questionReviewCategoriesService) {
		this.session = session;
		this.userService = userService;
		this.questionReviewCategoriesService = questionReviewCategoriesService;
	}

	/**
	 *
	 * @param model
	 * @return
	 *
	 *  Sessionに質問リストをセット
	 *  ホーム画面で値をセッションに登録している。
	 *  SessionHandlerInterceptorで指定のパス以外には、セッションが残らないように制御。
	 *  mvc-configにパラメーターはセットしてある。
	 *
	 */

	@GetMapping("/home")
	String getHome(Model model) {

		UserDto user = getAuthUser();

		List<QuestionReviewDto> questionReviewDtoList = questionReviewCategoriesService.findByUserId(user.getId());

		if(questionReviewDtoList.isEmpty()) {
			model.addAttribute("noQuestionMessage", "問題がまだ登録されていません。");
			model.addAttribute("newCount", 0);
			model.addAttribute("reviewCount", 0);
			return "index";
		}

		List<QuestionReviewDto> learnQuestionList =
				questionReviewCategoriesService.findByUserIdAndToday(user.getId());

		if(learnQuestionList.isEmpty()) {
			model.addAttribute("newCount", 0);
			model.addAttribute("reviewCount", 0);
			model.addAttribute("nextLearnDate", questionReviewDtoList.get(0).getReview().getNextLearnDate());
			model.addAttribute("compleateMessage", "本日の学習は完了しています。");
			return "index";
		}

		session.setAttribute("questions", learnQuestionList);
		int newCount = 0;
		int reviewCount = 0;

		for(QuestionReviewDto qrDto : learnQuestionList) {
			if(qrDto.getReview().getNextLearnDate() == null) {
				newCount ++;
			}else {
				reviewCount ++;
			}
		}

		model.addAttribute("firstQuestionId", questionReviewDtoList.get(0).getQuestion().getId());
		model.addAttribute("newCount", newCount);
		model.addAttribute("reviewCount", reviewCount);

		return "index";
	}

	@GetMapping("/")
	String getIndex() {
		return "redirect:/home";
	}

	private UserDto getAuthUser() {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String account = auth.getName();
		return userService.findByAccount(account);
	}

}
