SELECT
	    questions.id as id,
	    questions.question as question,
	    questions.answer as answer,
  	    questions.is_learned as is_learned,
  	    questions.user_id as user_id,
		reviews.repetitions as repetitions,
		reviews.intervals as intervals,
		reviews.easiness as easiness,
		reviews.compleate_days as compleate_days,
		reviews.next_learn_date as next_learn_date,
	    questions.created_date as created_date,
	    questions.updated_date as updated_date
FROM questions
INNER JOIN reviews
ON questions.id = reviews.id
INNER JOIN users
ON questions.user_id = users.id
WHERE questions.user_id = 1
ORDER BY next_learn_date DESC