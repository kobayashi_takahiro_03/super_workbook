<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.Date, java.text.DateFormat, java.text.SimpleDateFormat" %>
<%!
  /* 次回の学習日程がNullだったら今日の日付を入れる */
  private String GetDate() {
	Date date=new Date();
	DateFormat dateFormat=new SimpleDateFormat("yyyy/MM/dd HH:mm");
	String formattedDate=dateFormat.format(date);
	return formattedDate;
  }
%>
<jsp:include page="/WEB-INF/view/layout/layout.jsp">
	<jsp:param name="title" value="問題一覧" />
	<jsp:param name="content">
		<jsp:attribute name="value">
			<div class="container-fluid">
     			<div class="row">
         			<div class="col-sm-5 col-sm-offset-4 h-auto main">
					<div class="page-header">
						<h1>問題一覧</h1>
					</div>
					<a href="${pageContext.request.contextPath}/question/new" class="pull-right">新しい問題を作成する</a><br/>
						<c:forEach var="questionReview" items="${questionReviews}">
							<div class="question-contents">
								<span>問題内容：</span>
								<pre>${questionReview.question.question}</pre>
								<span>回答：</span>
								<pre>${questionReview.question.answer}</pre>
								<span>ステータス：${questionReview.question.isLearned.getString()},</span>
								<span>回答回数：${questionReview.review.repetitions}</span><br/>
								<span>カテゴリ：
									<c:forEach items="${questionReview.questionCategoryList}" var="category" varStatus="status">
									    ${category.category}<c:if test="${not status.last}">,</c:if>
									</c:forEach>
								</span><br/>
								<span>作成日時：<fmt:formatDate value="${questionReview.question.createdDate}"
																							pattern="yyyy/MM/dd" /></span><br/>
								<c:if test="${not empty questionReview.review.nextLearnDate}">
									<span>次回学習予定：<fmt:formatDate value="${questionReview.review.nextLearnDate}"
																										pattern="yyyy/MM/dd" /></span><br/>
								</c:if>
								<c:if test="${empty questionReview.review.nextLearnDate}">
									<span>次回学習予定：<%= GetDate()%></span><br/>
								</c:if>
								<a href="${pageContext.request.contextPath}/question/${questionReview.question.id}/edit" class="pull-right">
									編集
								</a><br/>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>
