<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/error/error.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>${param.title}</title>
		<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script src="<c:url value="/resources/js/script.js" />" type="text/javascript"></script>
	</head>
	<body>
	    	<h1>${status}&nbsp;${error}</h1>
		    <p>${message}</p>
		    <p>ログイン画面に戻ってください</p>
		    <form action="<c:url value='/login' />" method="post">
		    	<sec:csrfInput/>
		        <button class="btn btn-link" type="submit">ログイン画面に戻る</button>
		    </form>
	</body>
</html>