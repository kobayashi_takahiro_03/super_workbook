<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="/WEB-INF/view/layout/layout.jsp">
	<jsp:param name="title" value="カテゴリ新規作成" />
	<jsp:param name="content">
		<jsp:attribute name="value">
			<div class="container-fluid">
       			<div class="row">
           			<div class="col-sm-4 col-sm-offset-4	 h-auto main">
						<div class="page-header">
							<h1>カテゴリ編集</h1>
						</div>
						<c:if test="${not empty errorMessages }">
							<span>${errorMessages}</span>
						</c:if>
				        <form:form modelAttribute="categoryForm" method="post" class="form-horizontal"
				        				     action="${pageContext.request.contextPath}/category/${categoryForm.id}/edit">
				        	<sec:csrfInput/>
					        <form:errors path="*" element="div" cssClass="error-message-list" />
				            <form:input path="id" type="hidden" /> <br />
							<table class="table table-bordered table-hover">
			            		<tr>
						            	<th class="active col-sm-4"><form:label path="name" cssErrorClass="error-label">カテゴリ名：</form:label></th>
						            	<td class="form-group input-wrap"><form:input path="name"/></td>
						         </tr>
							</table><br/>
				            <form:button class="button" name="update" id="update">更新</form:button>
		   		            <form:button class="button" name="delete" id="delete">削除</form:button>
				        </form:form>
					</div>
				</div>
			</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>
