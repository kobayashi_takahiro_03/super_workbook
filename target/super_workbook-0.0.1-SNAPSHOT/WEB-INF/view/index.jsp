<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:include page="/WEB-INF/view/layout/layout.jsp">
	<jsp:param name="title" value="今日の問題" />
	<jsp:param name="content">
		<jsp:attribute name="value">
			<div class="container-fluid">
     			<div class="row">
         			<div class="col-sm-4 col-sm-offset-4 h-auto main">
						<div class="page-header">
							<h1>今日の問題</h1>
						</div>
						<div class="question">
							<p>新しく学習：${newCount}件</p>
							<p>復習：${reviewCount}件</p>
							<c:if test="${empty noQuestionMessage}">
								<c:if test= "${empty compleateMessage}">
									<a href="${pageContext.request.contextPath}/answer/${firstQuestionId}">学習する</a>
								</c:if>
								<c:if test="${not empty compleateMessage}">
									<p>${compleateMessage}</p>
									<p>次回学習日程：<fmt:formatDate value="${nextLearnDate}"
																			pattern="yyyy/MM/dd" /></p>
								</c:if>
							</c:if>
							<c:if test="${not empty noQuestionMessage}">
									<a href="${pageContext.request.contextPath}/question/new">問題を作成する</a>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>
