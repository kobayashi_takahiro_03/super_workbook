<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="/WEB-INF/view/layout/layout.jsp">
	<jsp:param name="title" value="マイページ" />
	<jsp:param name="content">
		<jsp:attribute name="value">
			<div class="container-fluid">
       			<div class="row">
           			<div class="col-sm-7 col-sm-offset-3	 h-auto main">
						<div class="page-header">
							<h1>マイページ</h1>
						</div>
							<form:form modelAttribute="userForm" method="post">
								<table class="table table-bordered table-hover">
									<tr>
									  	<th class="active col-sm-4"><form:label path="account" >アカウント:</form:label></th>
								        <td class="form-group input-wrap"><form:input path="account" readonly="true"/></td>
						            </tr>
				  					<tr>
									     <th class="active col-sm-4"><form:label path="password">パスワード:</form:label></th>
									     <td class="form-group input-wrap"><form:input path="password" type="password" readonly="true"/></td>
									</tr>
									<tr>
							            <th class="active col-sm-4"><form:label path="name">氏名:</form:label></th>
							            <td class="form-group input-wrap"><form:input path="name" readonly="true"/></td>
									</tr>
									<tr>
							            <th class="active col-sm-4"><form:label path="birthday">誕生日:</form:label></th>
							            <td class="form-group input-wrap"><form:input path="birthday" type="date" readonly="true"/></td>
									</tr>
									<tr>
							            <th class="active col-sm-4"><form:label path="mail">メールアドレス:</form:label></th>
										<td class="form-group input-wrap"><form:input path="mail" readonly="true"/> <br /></td>
									</tr>
									<tr>
							            <th class="active col-sm-4"><form:label path="roleName">権限:</form:label></th>
										<td class="form-group input-wrap"><form:input path="roleName" readonly="true"/> <br /></td>
									</tr>
								</table><br />
					        </form:form><br />
						 <a href="${pageContext.request.contextPath}/admin/${userForm.id}/edit" style="font-size:20px;">編集する</a>
					</div>
				</div>
			</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>