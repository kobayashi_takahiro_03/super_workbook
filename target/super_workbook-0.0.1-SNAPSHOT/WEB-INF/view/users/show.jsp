<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="/WEB-INF/view/layout/layout.jsp">
	<jsp:param name="title" value="マイページ" />
	<jsp:param name="content">
		<jsp:attribute name="value">
			<div class="container-fluid">
       			<div class="row">
           			<div class="col-sm-7 col-sm-offset-3	 h-auto main">
						<div class="page-header">
							<h1>マイページ</h1>
						</div>
							<form:form modelAttribute="userForm" method="post">
						        <form:errors path="*" element="div" cssClass="error-message-list" />
								<table class="table table-bordered table-hover">
									<tr>
									  	<th class="active col-sm-4"><form:label path="account" cssErrorClass="error-label">アカウント:</form:label></th>
								        <td class="form-group input-wrap"><form:input path="account" cssErrorClass="error-input" readonly="true"/></td>
						            </tr>
				  					<tr>
									     <th class="active col-sm-4"><form:label path="password" cssErrorClass="error-label">パスワード:</form:label></th>
									     <td class="form-group input-wrap"><form:input path="password" type="password" cssErrorClass="error-input" readonly="true"/></td>
									</tr>
									<tr>
							            <th class="active col-sm-4"><form:label path="name" cssErrorClass="error-label">氏名:</form:label></th>
							            <td class="form-group input-wrap"><form:input path="name" cssErrorClass="error-input" readonly="true"/></td>
									</tr>
									<tr>
							            <th class="active col-sm-4"><form:label path="birthday" cssErrorClass="error-label">誕生日:</form:label></th>
							            <td class="form-group input-wrap"><form:input path="birthday" type="date" cssErrorClass="error-input" readonly="true"/></td>
									</tr>
									<tr>
							            <th class="active col-sm-4"><form:label path="mail" cssErrorClass="error-label">メールアドレス:</form:label></th>
										<td class="form-group input-wrap"><form:input path="mail" cssErrorClass="error-input" readonly="true"/> <br /></td>
									</tr>
								</table><br />
					        </form:form><br />
						 <a href="${pageContext.request.contextPath}/user/${userForm.id}/edit" style="font-size:20px;">編集する</a>
					</div>
				</div>
			</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>